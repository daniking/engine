pub mod connection;
pub mod protocol;

#[derive(Debug)]
pub enum NetworkError {
    Io(std::io::ErrorKind),
    Deserialize(bincode::Error),
    ConnectFailed,
}
