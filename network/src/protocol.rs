use std::net::{SocketAddr, UdpSocket};

use socket2::{Domain, Protocol, Socket, Type};
use tracing::info;

pub fn bind_udp_socket(addr: SocketAddr) -> std::io::Result<UdpSocket> {
    common::span!(_guard, "net::bind_udp_socket");
    let domain = Domain::for_address(addr);
    let socket = Socket::new(domain, Type::DGRAM, Some(Protocol::UDP))?;
    socket.set_nonblocking(true)?;
    // socket.set_reuse_address(reuse) TODO: check this out
    let socket2_addr = addr.into();
    socket.bind(&socket2_addr)?;
    let udp_socket = socket.into();
    info!("Socket bound to {}", addr);
    Ok(udp_socket)
}
