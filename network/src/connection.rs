use std::net::{Ipv4Addr, SocketAddr, UdpSocket};

use serde::{de::DeserializeOwned, Serialize};
use tracing::info;

use crate::{protocol::bind_udp_socket, NetworkError};

const PACKET_BUFFER_SIZE: usize = 1024;

/// Represents a connection that can either send or receive packets.
///
/// Generic Types:
/// - `S` stands for Send, i.e the packet type that is sent
/// - `R` stands for Receive, i.e the packet type that is received
pub struct Connection<S: Serialize, R: DeserializeOwned> {
    pub(crate) sock: UdpSocket,
    _marker: std::marker::PhantomData<(S, R)>,
}

impl<S: Serialize, R: DeserializeOwned> Default for Connection<S, R> {
    fn default() -> Self {
        Self::bind_any().expect("Failed to bind socket")
    }
}
impl<S: Serialize, R: DeserializeOwned> Connection<S, R> {
    /// Connect to a remote host.
    ///
    /// This will bind a UDP socket to a random port and connect it to the remote host.
    pub fn connect(remote_addr: SocketAddr) -> Result<Self, NetworkError> {
        let con = Self::bind_any()?;
        info!("Connecting to {}", remote_addr);
        con.sock
            .connect(remote_addr)
            .map_err(|_| NetworkError::ConnectFailed)?;
        Ok(con)
    }

    /// Bind a UDP socket to a local address.
    pub fn bind(addr: SocketAddr) -> Result<Self, NetworkError> {
        let socket = bind_udp_socket(addr).expect("Failed to bind socket");
        Ok(Self {
            sock: socket,
            _marker: std::marker::PhantomData,
        })
    }

    pub fn bind_any() -> Result<Self, NetworkError> {
        Self::bind(SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(), 0))
    }

    /// Send a packet to the remote host that this connection was made to.
    pub fn send(&self, packet: &S) -> Result<(), NetworkError> {
        let packet = Self::serialize(packet);
        self.sock
            .send(&packet)
            .map_err(|e| NetworkError::Io(e.kind()))?;
        Ok(())
    }

    /// Send a packet to a specific endpoint.
    ///
    /// This is useful for sending packets to a host that we are not connected to.
    pub fn send_to(&self, packet: &S, addr: SocketAddr) -> Result<(), NetworkError> {
        let packet = Self::serialize(packet);
        self.sock
            .send_to(&packet, addr)
            .map_err(|e| NetworkError::Io(e.kind()))?;
        Ok(())
    }

    /// Receive a packet from the remote host.
    ///
    /// This is a blocking operation.
    pub fn recv(&self) -> Result<(R, SocketAddr), NetworkError> {
        let mut buf = [0; PACKET_BUFFER_SIZE];
        match self.sock.recv_from(&mut buf) {
            Ok((len, addr)) => Self::deserialize(&buf[..len]).map(|p| (p, addr)),
            Err(e) => Err(NetworkError::Io(e.kind())),
        }
    }

    pub fn addr(&self) -> SocketAddr {
        self.sock.local_addr().expect("Why would this fail?")
    }

    /// Serialize a packet.
    fn serialize<P: Serialize>(packet: &P) -> Vec<u8> {
        bincode::serialize(packet).expect("This should never fail")
    }
    /// Deserialize a packet.
    fn deserialize<P: DeserializeOwned>(bytes: &[u8]) -> Result<P, NetworkError> {
        match bincode::deserialize::<P>(bytes) {
            Ok(t) => Ok(t),
            Err(e) => Err(NetworkError::Deserialize(e)),
        }
    }
}

#[cfg(test)]
pub mod tests {

    // pub async fn create_network() -> (
    //     Connection<ClientPacket, ServerPacket>,
    //     Connection<ServerPacket, ClientPacket>,
    // ) {
    //     let addr = SocketAddr::from(([127, 0, 0, 1], 0));
    //     let server = Connection::bind(addr).await.unwrap();
    //     let client = Connection::connect(server.addr()).await.unwrap();

    //     (client, server)
    // }

    // async fn send_recv() {
    //     let (client, server) = create_network().await;

    //     for _ in 0..5 {
    //         let _ = client.send(&ClientPacket::Connect).await;
    //     }
    //     let mut connections = 0;

    //     loop {
    //         match server.try_recv() {
    //             Ok((ClientPacket::Connect, _)) => {
    //                 connections += 1;
    //             }
    //             Ok(_) => (),
    //             Err(_) => break,
    //         }
    //     }
    //     assert_eq!(connections, 5);
    // }
}
