pub mod cli;

use std::net::SocketAddr;

use common::time::Time;
use server::Server;

use crate::cli::ServerArgs;

fn main() {
    use clap::Parser;
    let args = ServerArgs::parse();
    common::setup_tracing();
    let mut time = Time::new();
    let addr = format!("{}:{}", args.addr, args.port);
    let addr = addr.parse::<SocketAddr>().expect("Failed to parse address");
    let mut server = Server::new(addr).expect("Failed to initialize server");
    loop {
        time.tick();
        server.tick(time.dt());
    }
}
