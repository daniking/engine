use clap::Parser;

#[derive(Debug, Parser)]
pub struct ServerArgs {
    #[clap(short, long)]
    pub port: u16,
    #[clap(short, long, default_value = "127.0.0.1")]
    pub addr: String,
}
