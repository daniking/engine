use network::NetworkError;

#[derive(Debug)]
pub enum Error {
    Network(NetworkError),
    Ecs,
    // TODO: add more errors
}

impl From<NetworkError> for Error {
    fn from(value: NetworkError) -> Self {
        Self::Network(value)
    }
}

impl From<apecs::anyhow::Error> for Error {
    fn from(_value: apecs::anyhow::Error) -> Self {
        Self::Ecs
    }
}
