pub mod error;

use std::{collections::{HashMap, HashSet}, net::SocketAddr, time::Duration};

use apecs::{anyhow, Components};
use common::{
    comp::{BlockPos, MoveControl, Player},
    math::{Vec3, Vec2},
    net::packet::{ClientPacket, PingKind, PlayerListAction, ServerPacket},
    resources::{GameMode, PlayerEntity},
    sys::{input::update_input, physics::update_physics},
    uid::Uid,
    world::terrain::TerrainGrid,
    State,
};
use error::Error;
use network::connection::Connection;
use tracing::{info, warn};

const RENDER_DIST: u32 = 4;

pub struct Client {
    /// All the players in the world
    players: HashMap<Uid, Player>,
    /// The ecs state
    state: State,
    /// A connection to the server
    con: Connection<ClientPacket, ServerPacket>,
    // Player uid
    uid: Uid,
    last_ping_time: f64,
    last_pong_time: f64,
    // The time that has passed since the last ping was sent
    // and the last pong arrived. It represents the latency of the connection.
    last_ping_delta: f64,
    pending_chunks: HashSet<Vec2<i32>>
}

impl Client {
    pub fn new(mode: GameMode, remote_addr: SocketAddr) -> Result<Self, Error> {
        let con = Connection::connect(remote_addr)?;
        let mut state = Self::setup_client_world(mode)?;
        con.send(&ClientPacket::Connect)?;
        info!("Waiting for initial sync...");
        let player_uid = loop {
            if let Ok((packet, _)) = con.recv() {
                match packet {
                    ServerPacket::PlayerSync {
                        spawn,
                        velocity,
                        control,
                        uid,
                    } => {
                        let entity = state
                            .world_mut()
                            .entity_with_bundle((spawn, velocity, control, uid));
                        state.write_resource(PlayerEntity(Some(entity)));
                        break uid;
                    }
                    _ => continue, // Wait for the initial sync
                }
            }
        };
        let this = Self {
            state,
            players: HashMap::new(),
            con,
            uid: player_uid,
            last_ping_time: 0.0,
            last_pong_time: 0.0,
            last_ping_delta: 0.0,
            pending_chunks: HashSet::new(),
        };
        Ok(this)
    }

    pub fn setup_client_world(mode: GameMode) -> Result<State, anyhow::Error> {
        let mut state = State::new(mode);
        state.world_mut().with_system_with_dependencies(
            "input_update",
            update_input,
            &[],
            &["update_physics"],
        )?;
        state
            .world_mut()
            .with_system("update_physics", update_physics)?;
        Ok(state)
    }

    /// Execute a single client tick.
    pub fn tick(&mut self, dt: Duration, control: MoveControl) {
        common::span!(_guard, "Client::tick");
        self.handle_input(control);
        self.handle_server_packets();
        self.state.tick(dt);

        // Handle terrain updates
        let mut chunks_to_remove = Vec::new();
        let pos = self.state.world_mut().query::<&BlockPos>().find_one(0).cloned();

        if let Some(pos) = pos {
            let player_chunk_pos = TerrainGrid::chunk_pos(pos.0.map(|f| f as i32));
            self.state.terrain().iter().for_each(|(pos, _)| {
                let distance = (player_chunk_pos - *pos).map(|x| (x.abs() as u32));
                let dist_sqrd = distance.x.pow(2) + distance.y.pow(2);
                if dist_sqrd > 4 {
                    chunks_to_remove.push(*pos);
                }
            });

            for chunk in chunks_to_remove {
                self.state.remove_chunk(chunk);

                let half = RENDER_DIST / 2;
                let player_pos = player_chunk_pos.map(|x| x as u32);
                // new boundaries
                let start_x = player_pos.x - half;
                let end_x = player_pos.x + half;
                let start_z = player_pos.y - half;
                let end_z = player_pos.y + half;
                
                for x in start_x..end_x {
                    for z in start_z..end_z {
                        let pos = Vec2::new(x, z).map(|f| f as i32);
                        if !self.pending_chunks.contains(&pos) {
                            let _ = self.con.send(&ClientPacket::LoadChunk { pos });
                            self.pending_chunks.insert(pos);
                        }
                    }
                }
            }
            self.pending_chunks.clear(); // this is just for debugging
        }

        if self.state.get_time() - self.last_ping_time > 1.0 {
            let _ = self.con.send(&ClientPacket::Ping(PingKind::Ping));
            self.last_ping_time = self.state.get_time();
        }
    }

    pub fn handle_input(&mut self, ctrl: MoveControl) {
        // Pass client input to player's entity
        // info!("My id: {}", self.uid);
        // self is always uid 0
        self.state.write_component(0, ctrl);

        let _ = self.con.send(&ClientPacket::ControlUpdate {
            uid: self.uid,
            control: ctrl,
        });

        if let Some(pos) = self.state.components().get_component::<BlockPos>(0)
        // self is always uid 0
        {
            let _ = self.con.send(&ClientPacket::PlayerUpdate {
                uid: self.uid,
                pos: *pos,
            });
        }
    }
    pub fn handle_server_packets(&mut self) {
        if let Ok((packet, _)) = self.con.recv() {
            match packet {
                ServerPacket::PlayerListUpdate(update) => match update {
                    PlayerListAction::Sync(list) => {
                        self.players = list;
                    }
                    PlayerListAction::Add(id, player) => {
                        let old_player = self.players.insert(id, player);
                        if old_player.is_some() {
                            warn!("Player with id {} already exists! Overwriting...", id);
                        }
                        self.state
                            .world_mut()
                            .entity_with_bundle((Vec3::zero().with_y(257.0), id));
                    }
                    PlayerListAction::Remove(entity_id) => {
                        if self.players.remove(&entity_id).is_none() {
                            warn!("Player {} was not on the player list.", entity_id);
                        }
                    }
                },
                ServerPacket::Ping(kind) => match kind {
                    PingKind::Ping => {}
                    PingKind::Pong => {
                        // info!("Pong!");
                        self.last_pong_time = self.state.get_time();
                        self.last_ping_delta = self.state.get_time() - self.last_ping_time;
                    }
                },
                ServerPacket::PlayerUpdate { uid, pos } => {
                    if uid == self.uid {
                        return;
                    }
                    // Find if there is an entity that has the same uid as the one we received
                    // This might be None if the player just joined the server
                    let entity = self
                        .state
                        .world_mut()
                        .query::<&Uid>()
                        .iter_mut()
                        .filter(|id| *id.value() == uid)
                        .map(|entry| entry.id())
                        .last();

                    if entity.is_none() {
                        self.state
                            .world_mut()
                            .entity_with_bundle((Vec3::zero().with_y(257.0), uid));
                    }

                    if let Some(entity) = entity {
                        self.state
                            .world_mut()
                            .resource_mut::<Components>()
                            .unwrap()
                            .insert_component(entity, pos);
                    }
                }
                _ => (),
            }
        }
    }

    pub fn client_id(&self) -> Uid {
        self.uid
    }

    pub fn ping_ms(&self) -> f64 {
        self.last_ping_delta * 1000.0
    }

    pub fn state(&self) -> &State {
        &self.state
    }

    pub fn state_mut(&mut self) -> &mut State {
        &mut self.state
    }

    pub fn player_count(&self) -> usize {
        self.players.len()
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        common::span!(_guard, "Client::drop");
        if let Err(e) = self.con.send(&ClientPacket::Disconnect(self.uid)) {
            tracing::error!("Failed to send disconnect packet: {:?}", e);
        }
    }
}
