use network::NetworkError;

#[derive(Debug)]
pub enum Error {
    Network(NetworkError),
}

impl From<NetworkError> for Error {
    fn from(e: NetworkError) -> Self {
        Self::Network(e)
    }
}
