use apecs::{anyhow::Result, ok, Components, Entities, Read, ShouldContinue, Write};
use common::{
    comp::{BlockPos, MoveControl, Player, Velocity},
    math::Vec3,
    net::packet::{ClientPacket, PingKind, PlayerListAction, ServerPacket},
    uid::{EntityMap, Uid},
};
use std::{collections::HashMap, net::SocketAddr};
use tracing::info;

use crate::{client::RemoteClient, ServerConnection};

pub fn handle_new_packets(
    (con, entities, mut comps, mut entity_map): (
        Read<ServerConnection>,
        Write<Entities>,
        Write<Components>,
        Write<EntityMap>,
    ),
) -> Result<ShouldContinue> {
    match con.recv() {
        Result::Ok((packet, addr)) => match packet {
            ClientPacket::Connect => {
                let new_player_id =
                    initialize_client(addr, &con, entities, &mut comps, &mut entity_map);
                let mut query = comps.query::<&RemoteClient>();

                let players = query
                    .iter_mut()
                    .map(|client| (client.uid, Player { addr: client.addr }))
                    .collect::<HashMap<_, _>>();

                // Send the new player to all the other players
                for (uid, player) in players.iter() {
                    if new_player_id == *uid {
                        continue;
                    }
                    let _ = con.send_to(
                        &ServerPacket::PlayerListUpdate(PlayerListAction::Add(
                            new_player_id,
                            Player { addr },
                        )),
                        player.addr,
                    );
                }
                // Send all the other players to the new player
                let _ = con.send_to(
                    &ServerPacket::PlayerListUpdate(PlayerListAction::Sync(players)),
                    addr,
                );
                info!("Player from: {} has joined!", addr);
            }

            ClientPacket::Disconnect(uid) => handle_disconnect(uid, &mut comps, &con, entities),
            ClientPacket::ControlUpdate { uid, control } => {
                let maybe_control = comps.get_component_mut::<MoveControl>(uid.0 as usize);
                if let Some(mut old_control) = maybe_control {
                    *old_control = control;
                }
            }
            ClientPacket::Ping(kind) => match kind {
                PingKind::Ping => {
                    let _ = con.send_to(&ServerPacket::Ping(PingKind::Pong), addr);
                }
                PingKind::Pong => {}
            },
            ClientPacket::PlayerUpdate { uid, pos } => {
                comps.insert_component(uid.0 as usize, pos);
                for client in comps.query::<&RemoteClient>().iter_mut() {
                    if client.uid == uid {
                        continue;
                    }
                    let packet = ServerPacket::PlayerUpdate { uid, pos };
                    let _ = con.send_to(&packet, client.addr);
                }
            },
            ClientPacket::LoadChunk { pos } => {
                info!("Client requested chunk at: {:?}", pos);
            }
        },
        Result::Err(_e) => {}
    }
    ok()
}

fn handle_disconnect(
    uid: Uid,
    comps: &mut Write<Components>,
    con: &ServerConnection,
    entities: Write<Entities>,
) {
    if comps
        .get_component::<RemoteClient>(uid.0 as usize)
        .is_none()
    {
        return;
    }
    if let Some(entity) = entities.hydrate(uid.0 as usize) {
        entities.destroy(entity);
        for client in comps.query::<&RemoteClient>().iter_mut() {
            let _ = con.send_to(
                &ServerPacket::PlayerListUpdate(PlayerListAction::Remove(uid)),
                client.addr,
            );
        }
    }
    info!("Player {} disconnected", uid);
}
fn initialize_client(
    addr: SocketAddr,
    con: &ServerConnection,
    mut entities: Write<Entities>,
    comps: &mut Write<Components>,
    _entity_map: &mut Write<EntityMap>,
) -> Uid {
    let entity = entities.create();
    let uid = Uid(entity.id() as u64);
    let client = RemoteClient { uid, addr };
    let spawn_point = BlockPos(Vec3::zero().with_y(258.0)); // TODO: Make this a resource, im lazy rn
    let velocity = Velocity(Vec3::default());
    let move_control = MoveControl::default();
    // TODO: maybe update entity map?
    // let uid = new_entity_sync(&mut entities, &mut entity_map, |ent, uid| {
    //     ent.insert_component(RemoteClient {addr, uid: *uid});
    //     ent.insert_component(spawn_point);
    //     ent.insert_component(velocity);
    //     ent.insert_component(move_control);
    // });
    comps.insert_bundle(
        uid.0 as usize,
        (client, spawn_point, velocity, move_control),
    );
    // TODO: handle error?
    let _ = con.send_to(
        &ServerPacket::PlayerSync {
            uid,
            spawn: spawn_point,
            velocity,
            control: move_control,
        },
        addr,
    );
    uid
}
