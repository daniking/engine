use apecs::{Entities, Entity, Write};
use common::uid::{EntityMap, Uid};

pub mod packet;

/// Incrementally add an entity to the map, returning the uid
pub fn new_entity_sync(
    entities: &mut Entities,
    mappings: &mut Write<EntityMap>,
    register_comps: impl FnOnce(&mut Entity, &Uid),
) -> Uid {
    let mut entity = entities.create();
    let uid = mappings.peek_next();
    entity.insert_component(uid);
    // Register extra components before adding it to the map
    register_comps(&mut entity, &uid);
    mappings.add_inc(entity);
    uid
}
