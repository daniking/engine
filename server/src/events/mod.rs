mod connect;

use std::{collections::VecDeque, net::SocketAddr};

#[derive(Default)]
pub struct EventEmitter {
    events: VecDeque<ServerEvent>,
}

impl EventEmitter {
    pub fn emit(&mut self, event: ServerEvent) {
        self.events.push_back(event);
    }

    pub fn recv(&mut self) -> impl ExactSizeIterator<Item = ServerEvent> {
        std::mem::take(&mut self.events).into_iter()
    }
}
pub enum ServerEvent {
    PlayerJoined(SocketAddr, usize),
}

use crate::Server;

use self::connect::handle_connect;

impl Server {
    pub fn handle_events(&mut self) {
        common::span!(_guard, "Server::handle_events");
        let events = self.state.read_resource_mut::<EventEmitter>();

        for event in events.recv() {
            match event {
                ServerEvent::PlayerJoined(_s, _addr) => handle_connect(self),
            }
        }
    }
}
