use std::collections::VecDeque;

use common::net::packet::ServerPacket;

pub struct PacketQueue {
    queue: VecDeque<ServerPacket>,
}

impl PacketQueue {
    pub fn new() -> Self {
        Self {
            queue: VecDeque::new(),
        }
    }

    pub fn enqueue(&mut self, packet: ServerPacket) {
        self.queue.push_back(packet);
    }

    pub fn dequeue(&mut self) -> Option<ServerPacket> {
        self.queue.pop_front()
    }
}
