pub mod client;
pub mod error;
pub mod events;
pub mod queue;
pub mod sys;

use std::{net::SocketAddr, time::Duration};

use apecs::anyhow::{self, Ok, Result};

use common::{
    comp::BlockPos,
    math::{Vec2, Vec3},
    net::packet::{ClientPacket, ServerPacket},
    resources::GameMode,
    world::terrain::TerrainGrid,
    State,
};
use network::connection::Connection;
use queue::PacketQueue;
use tracing::info;

use crate::{error::Error, events::EventEmitter};

pub type ServerConnection = Connection<ServerPacket, ClientPacket>;

pub struct Server {
    state: State,
}

impl Server {
    pub fn new(addr: SocketAddr) -> Result<Self, Error> {
        let connect = Connection::bind(addr)?;
        let state = Self::setup_server_world(connect).expect("Failed to setup world");
        let this = Self { state };
        Result::Ok(this)
    }

    pub fn setup_server_world(
        connection: Connection<ServerPacket, ClientPacket>,
    ) -> Result<State, anyhow::Error> {
        let mut state = State::new(GameMode::Server);
        state.world_mut().with_resource(connection)?;
        state.world_mut().with_resource(EventEmitter::default())?;
        state.world_mut().with_resource(PacketQueue::new())?;
        state
            .world_mut()
            .with_system("handle_new_packets", sys::packet::handle_new_packets)?;
        Ok(state)
    }

    pub fn tick(&mut self, dt: Duration) {
        common::span!(_guard, "Server::tick");
        self.state.tick(dt);
        let terrain = self.state.terrain();
        let conn = self.state.world().resource::<ServerConnection>().unwrap();
        for pos in self.state.world_mut().query::<&BlockPos>().iter_mut() {
            let pos = pos.0.map(|x| x as i32);
            let chunk_pos = TerrainGrid::chunk_pos(pos);
        }
    }

    pub fn addr(&self) -> SocketAddr {
        self.state
            .world()
            .resource::<Connection<ServerPacket, ClientPacket>>()
            .unwrap()
            .addr()
    }
}
