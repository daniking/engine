use std::net::SocketAddr;

use common::{net::packet::ServerPacket, uid::Uid};

/// Represents a Client connected to the server
pub struct RemoteClient {
    pub uid: Uid,
    pub addr: SocketAddr,
}

impl RemoteClient {
    pub async fn send(&self, _p: &ServerPacket) {}
}
