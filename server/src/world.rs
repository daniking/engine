use common::{world::{chunk::CHUNK_SIZE, block::{Block, BlockId}}, math::Vec2};
use noise::{BasicMulti, Perlin, MultiFractal, NoiseFn};

pub struct World {
    perlin: BasicMulti<Perlin>,
}

impl World {

    pub fn new(seed: u32) -> Self {
        Self {
            perlin: BasicMulti::new(seed)
            .set_octaves(7),
        }
    }

    pub fn generate_chunk(&mut self, pos: Vec2<i32>) {
        let mut blocks = vec![];
        for x in 0..CHUNK_SIZE.x {
            for y in 0..CHUNK_SIZE.y {
                for z in 0..CHUNK_SIZE.z {
                    let world_x = x as f64 + pos.x as f64 * CHUNK_SIZE.x as f64 + 221.0;
                    let world_z = z as f64 + pos.y as f64 * CHUNK_SIZE.z as f64 + 221.0;
                    let scale = 1000.0;
                    // map the noise to a range of 0 to 1

                    let max_height = common::math::map(
                        self.perlin.get([world_x / scale, world_z / scale]),
                        -1.0,
                        1.0,
                        0.0,
                        1.0,
                    ) * 256.0;

                    let stone_max_height = common::math::map(
                        self.perlin.get([world_x / scale, world_z / scale]),
                        -1.0,
                        1.0,
                        0.0,
                        1.0,
                    ) * 256.0;

                    let stone_height = (stone_max_height * max_height) / 256.0;

                    let max_height = max_height as u32;

                    if y < stone_height as u32 {
                        blocks.push(Block::new(BlockId::Stone));
                    } else if y < max_height {
                        blocks.push(Block::new(BlockId::Dirt));
                    } else if y == max_height {
                        blocks.push(Block::new(BlockId::GrassTop));
                    } else {
                        blocks.push(Block::new(BlockId::Air));
                    }
                }
            }
        }
    }
}