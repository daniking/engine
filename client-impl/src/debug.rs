use common::{math::Vec3, resources::GameMode};

pub struct DebugInfo {
    pub framerate: f32,
    pub camera_dir: Vec3<f32>,
    pub camera_pos: Vec3<f32>,
    pub vertex_count: u32,
    pub ping: f64,
    pub mode: GameMode,
}

impl DebugInfo {
    pub fn camera_target(&self) -> &str {
        let (x, y, z) = self.camera_dir.map(|f| f.abs()).into_tuple();
        if x >= y && x >= z {
            return if self.camera_dir.x > 0.0 { "+X" } else { "-X" };
        }
        if y >= z {
            return if self.camera_dir.y > 0.0 { "+Y" } else { "-Y" };
        }
        if self.camera_dir.z > 0.0 {
            "+Z"
        } else {
            "-Z"
        }
    }
}
