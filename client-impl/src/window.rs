use common::math::Vec2;

use crate::{
    error::Error,
    event::{EventLoop, GameInput, InputEvent, WindowEvent},
    render::renderer::Renderer,
};

type Key = winit::event::VirtualKeyCode;

/// Represents a window.
pub struct Window {
    /// The actual window implementation.
    winit_impl: winit::window::Window,
    /// A buffer of events that have been collected since the last call to [`Window::take_events`].
    events: Vec<WindowEvent>,
    renderer: Renderer,
    /// This flag is set to true when the window is resized.
    resized: bool,
}

impl Window {
    /// Builds a new [Window].
    ///
    /// It uses a [winit::window::Window] internally and creates an [EventLoop]
    /// without running it. The [Window] is returned along with the [EventLoop].
    /// The returned value is wrapped on [Result] which means the creation may fail.
    pub fn new() -> Result<(Self, EventLoop), Error> {
        let event_loop: winit::event_loop::EventLoop<()> = EventLoop::new();
        let builder = winit::window::WindowBuilder::new()
            .with_title("Engine")
            .with_inner_size(winit::dpi::LogicalSize::new(800, 600));

        let winit_impl = builder.build(&event_loop).expect("Could not build window");
        let renderer = Renderer::new(&winit_impl)?;
        let this = Self {
            winit_impl,
            events: Vec::new(),
            renderer,
            resized: false,
        };
        Ok((this, event_loop))
    }
    /// Set grabbing mode on the cursor preventing it from leaving the window.
    /// Aditionally, the cursor visibility is the opposite of the grab value.
    /// (i.e. if grab is true, the cursor is hidden, and vice versa)
    pub fn grab_cursor(&mut self, grab: bool) {
        self.winit_impl.set_cursor_visible(!grab);
        let mode = if grab {
            winit::window::CursorGrabMode::Locked
        } else {
            winit::window::CursorGrabMode::None
        };
        if let Err(e) = self.winit_impl.set_cursor_grab(mode) {
            tracing::warn!("Could not grab cursor in {:?} mode ({})", mode, e);
        }
    }

    /// Collects the given [winit::event::WindowEvent], mapping it to a [crate::window::WindowEvent].
    /// The mapped event is pushed to the internal event buffer.
    ///
    /// This method is intended to be called from the winit event loop.
    pub fn collect_window_event(&mut self, event: winit::event::WindowEvent) {
        match event {
            winit::event::WindowEvent::CloseRequested => self.events.push(WindowEvent::Close),
            winit::event::WindowEvent::Resized(e) => {
                self.resized = true;
                self.events
                    .push(WindowEvent::Resize(Vec2::new(e.width, e.height)));
            }

            winit::event::WindowEvent::KeyboardInput { input, .. } => {
                let input_key = match input.virtual_keycode {
                    Some(key) => InputEvent::Key(key),
                    None => InputEvent::ScanCode(input.scancode),
                };
                if let Some(game_input) = Self::input_from_event(input_key) {
                    let event = WindowEvent::Input(
                        game_input,
                        input.state == winit::event::ElementState::Pressed,
                    );
                    self.events.push(event);
                }
            }

            winit::event::WindowEvent::MouseInput { state, button, .. } => {
                if let Some(game_input) = Self::input_from_event(InputEvent::Mouse(button)) {
                    let event = WindowEvent::Input(
                        game_input,
                        state == winit::event::ElementState::Pressed,
                    );
                    self.events.push(event);
                }
            }
            _ => (),
        }
    }

    /// Collects the given [winit::event::DeviceEvent], mapping it to a [crate::window::WindowEvent].
    /// The mapped event is pushed to the internal event buffer.
    ///
    /// This method is intended to be called from the winit event loop.
    pub fn collect_device_event(&mut self, event: winit::event::DeviceEvent) {
        if let winit::event::DeviceEvent::MouseMotion { delta: (dx, dy) } = event {
            let delta = Vec2::new(dx as f32, dy as f32);
            //  Handle sensitivity and mouse inversion
            self.events.push(WindowEvent::CursorMove(delta));
        }
    }

    /// Takes the internal event buffer, returning it and replacing it with an empty one.
    ///
    /// This is used for further processing events events that have been collected since the last call.
    pub fn take_events(&mut self) -> Vec<WindowEvent> {
        if self.resized {
            self.resized = false;
            self.renderer.resize(self.dimensions());
        }
        std::mem::take(&mut self.events)
    }

    pub fn renderer(&self) -> &Renderer {
        &self.renderer
    }

    pub fn renderer_mut(&mut self) -> &mut Renderer {
        &mut self.renderer
    }

    pub fn dimensions(&self) -> Vec2<u32> {
        let size = self.winit_impl.inner_size();
        Vec2::new(size.width, size.height)
    }

    fn input_from_event(key: InputEvent) -> Option<GameInput> {
        let input = match key {
            InputEvent::Key(Key::W) => GameInput::MoveForward,
            InputEvent::Key(Key::S) => GameInput::MoveBackward,
            InputEvent::Key(Key::A) => GameInput::MoveLeft,
            InputEvent::Key(Key::D) => GameInput::MoveRight,
            InputEvent::Key(Key::Space) => GameInput::Jump,
            InputEvent::Key(Key::LShift) => GameInput::Sneak,
            InputEvent::Key(Key::Comma) => GameInput::CursorTrapToggle,
            InputEvent::Key(Key::F12) => GameInput::F12,
            _ => return None,
        };
        Some(input)
    }

    pub fn scale_factor(&self) -> f32 {
        self.winit_impl.scale_factor() as f32
    }

    pub fn window_impl(&self) -> &winit::window::Window {
        &self.winit_impl
    }
}
