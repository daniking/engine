use clap::Parser;

#[derive(Debug, Parser)]
pub struct ClientArgs {
    #[clap(short, long, default_value = "0")]
    pub port: u16,
    #[clap(short, long, default_value = "127.0.0.1")]
    pub addr: Option<String>,
    #[command(subcommand)]
    pub mode: GameMode,
}

#[derive(Parser, Debug, Clone, PartialEq, Eq)]
pub enum GameMode {
    /// Singleplayer mode, i.e the server and client are running on the same machine.
    Singleplayer,
    /// Client mode, i.e the client is connecting to a server.
    Client,
}

impl From<GameMode> for common::resources::GameMode {
    fn from(mode: GameMode) -> Self {
        match mode {
            GameMode::Singleplayer => Self::Singleplayer,
            GameMode::Client => Self::Client,
        }
    }
}
