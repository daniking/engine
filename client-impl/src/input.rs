use common::math::Vec3;

#[derive(Default, Debug)]
pub struct InputController {
    pub right: bool,
    pub left: bool,
    pub forward: bool,
    pub backward: bool,
    pub up: bool,
    pub down: bool,
}

impl InputController {
    /// Returns a vector representing the direction of the input.
    pub fn dir(&self) -> Vec3<f32> {
        let x = if self.right { 1.0 } else { 0.0 } + if self.left { -1.0 } else { 0.0 };
        let y = if self.up { 1.0 } else { 0.0 } + if self.down { -1.0 } else { 0.0 };
        let z = if self.forward { 1.0 } else { 0.0 } + if self.backward { -1.0 } else { 0.0 };
        Vec3::new(x, y, z)
    }
}
