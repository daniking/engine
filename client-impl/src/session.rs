use std::net::SocketAddr;

use crate::{
    camera::{self, Camera},
    debug::DebugInfo,
    event::{GameInput, WindowEvent},
    input::InputController,
    render::{
        buffer::{Buffer, GlobalBuffer},
        globals::{Globals, GlobalsBindGroup},
        mesh::{Mesh, Quad},
        pipelines::player::PlayerVertex,
        renderer::{compute_cube_index_buf_u16, frame::RenderFrame, Renderer},
    },
    state::{State, StateResult},
    terrain::Terrain,
    App,
};
use client::Client;
use common::{
    comp::{BlockPos, MoveControl},
    math::Vec3,
    resources::GameMode,
};
pub struct SessionState {
    input: InputController,
    camera: Camera,
    globals: GlobalBuffer<Globals>,
    global_binding: GlobalsBindGroup,
    terrain: Terrain,
    client: Client,
    wireframe: bool,
    /// Locks the session by stopping the camera from moving and releasing the cursor.
    locked_session: bool,
    player_buffers: Option<Buffer<PlayerVertex>>,
    player_indices: Option<Buffer<u16>>,
}

impl SessionState {
    pub fn new(mode: GameMode, addr: SocketAddr, renderer: &mut Renderer) -> Self {
        let dims = renderer.dimensions().map(|d| d as f32);
        let globals = renderer.create_global_buffer(&[Globals::default()]);
        let client = Client::new(mode, addr).expect("Failed to initialize client");
        Self {
            input: InputController::default(),
            camera: Camera::new(dims.x / dims.y),
            client,
            global_binding: renderer.bind_globals(&globals),
            globals,
            terrain: Terrain::new(),
            wireframe: false,
            locked_session: false,
            player_buffers: None,
            player_indices: None,
        }
    }

    fn update_globals(&self, app: &mut App, globals: Globals) {
        app.window
            .renderer()
            .update_binding_buffer(&self.globals, &[globals]);
    }

    fn handle_client_meshes(&mut self, renderer: &mut Renderer) {
        let mut vertices = vec![];

        for id in 0..self.client.player_count() {
            if id == 0 {
                continue; // Skip self
            }

            if let Some(pos) = self
                .client
                .state()
                .components()
                .get_component::<BlockPos>(id)
            {
                let (x, y, z) = pos.0.into_tuple();
                let mesh = create_player_model(Vec3::new(x, y, z));
                vertices.push(mesh.vertices().to_vec());
            }
        }

        // Flatten the nested vector into a single vector
        let flattened_vertices: Vec<PlayerVertex> = vertices.into_iter().flatten().collect();

        // Create the vertex buffer using the flattened vertices
        self.player_buffers = Some(renderer.create_vertex_buffer(&flattened_vertices));
        self.player_indices = Some(compute_cube_index_buf_u16(
            renderer.device.handle(),
            flattened_vertices.len(),
        ));
    }
}

impl State for SessionState {
    fn init(&mut self) {}

    fn tick(&mut self, app: &mut App, events: Vec<WindowEvent>) -> StateResult {
        let dt = app.time.dt().as_secs_f32();

        // Set camera pos to player pos
        // entity 0 is always the player
        // TODO: find entity by uid
        if let Some(pos) = self
            .client
            .state()
            .components()
            .get_component::<BlockPos>(0)
        {
            self.camera.set_pos(pos.0);
        }

        self.handle_client_meshes(app.window.renderer_mut());

        for event in events {
            if let WindowEvent::Resize(dims) = event {
                let dims = dims.map(|d| d as f32);
                self.camera.set_aspect_ratio(dims.x / dims.y);
            }

            if let WindowEvent::Close = event {
                return StateResult::Exit;
            }
            if let WindowEvent::CursorMove(delta) = event {
                let dx = delta.x * dt * 90.0;
                let dy = delta.y * dt * 90.0;
                self.camera.rotate(dx, -dy);
            }

            if let WindowEvent::Input(input, pressed) = event {
                match input {
                    GameInput::MoveForward => self.input.forward = pressed,
                    GameInput::MoveBackward => self.input.backward = pressed,
                    GameInput::MoveLeft => self.input.left = pressed,
                    GameInput::MoveRight => self.input.right = pressed,
                    GameInput::Jump => self.input.up = pressed,
                    GameInput::Sneak => self.input.down = pressed,
                    GameInput::CursorTrapToggle => {
                        if pressed {
                            self.locked_session = !self.locked_session;
                            app.window.grab_cursor(!self.locked_session);
                        }
                    }

                    GameInput::F12 => {
                        if pressed {
                            self.wireframe = !self.wireframe;
                        }
                    }
                }
            }
        }
        let mode = self.client.state().read_resource::<GameMode>().clone();
        let debug_info = DebugInfo {
            framerate: dt,
            camera_dir: self.camera.looking_dir(),
            camera_pos: self.camera.pos(),
            vertex_count: self.terrain.v_count(),
            ping: self.client.ping_ms(),
            mode,
        };

        app.egui_state.draw(app.window.window_impl(), debug_info);

        if self.locked_session {
            return StateResult::Continue;
        }

        let dir_vec = self.input.dir();
        let vectors = (self.camera.forward(), self.camera.right());
        let move_direction =
            vectors.0 * dir_vec.z + Vec3::unit_y() * dir_vec.y + vectors.1 * -dir_vec.x;

        self.camera.compute_matrices();

        let camera::Matrices { view, proj } = self.camera.matrices();
        let control = MoveControl {
            dir: move_direction,
        };

        self.terrain.tick(app.window.renderer_mut());
        self.client.tick(app.time.dt(), control);
        self.update_globals(app, Globals::new(view, proj));
        StateResult::Continue
    }

    fn render(&self, frame: &mut RenderFrame) {
        let mut pass = frame.make_pass();
        self.terrain.render(&mut pass, self.wireframe);
        if let Some(buf) = &self.player_buffers {
            if let Some(indices) = &self.player_indices {
                pass.render_player(indices, buf);
            }
        }
    }

    fn binding(&self) -> &GlobalsBindGroup {
        &self.global_binding
    }

    fn enable_gui(&self) -> bool {
        true
    }
}

fn create_player_model(offs: Vec3<f32>) -> Mesh<PlayerVertex> {
    let mut mesh = Mesh::new();
    mesh.push_quad(Quad::new(
        PlayerVertex::new(Vec3::new(-0.5, -0.5, -0.5) + offs, Vec3::new(0.3, 0.5, 0.6)),
        PlayerVertex::new(Vec3::new(-0.5, 0.5, -0.5) + offs, Vec3::new(1.0, 0.5, 0.0)),
        PlayerVertex::new(Vec3::new(-0.5, 0.5, 0.5) + offs, Vec3::new(0.7, 0.2, 0.3)),
        PlayerVertex::new(Vec3::new(-0.5, -0.5, 0.5) + offs, Vec3::new(0.3, 0.5, 0.3)),
    ));
    // +x
    mesh.push_quad(Quad::new(
        PlayerVertex::new(Vec3::new(0.5, -0.5, 0.5) + offs, Vec3::new(0.3, 0.3, 0.3)),
        PlayerVertex::new(Vec3::new(0.5, 0.5, 0.5) + offs, Vec3::new(1.0, 0.5, 0.0)),
        PlayerVertex::new(Vec3::new(0.5, 0.5, -0.5) + offs, Vec3::new(0.5, 0.5, 0.0)),
        PlayerVertex::new(Vec3::new(0.5, -0.5, -0.5) + offs, Vec3::new(0.3, 0.5, 0.3)),
    ));
    // -y
    mesh.push_quad(Quad::new(
        PlayerVertex::new(Vec3::new(0.5, -0.5, -0.5) + offs, Vec3::new(0.0, 0.5, 0.4)),
        PlayerVertex::new(Vec3::new(-0.5, -0.5, -0.5) + offs, Vec3::new(0.8, 0.5, 0.0)),
        PlayerVertex::new(Vec3::new(-0.5, -0.5, 0.5) + offs, Vec3::new(0.3, 0.5, 0.9)),
        PlayerVertex::new(Vec3::new(0.5, -0.5, 0.5) + offs, Vec3::new(0.5, 0.5, 0.1)),
    ));
    // +y
    mesh.push_quad(Quad::new(
        PlayerVertex::new(Vec3::new(0.5, 0.5, 0.5) + offs, Vec3::new(0.4, 0.5, 0.9)),
        PlayerVertex::new(Vec3::new(-0.5, 0.5, 0.5) + offs, Vec3::new(0.3, 0.5, 0.8)),
        PlayerVertex::new(Vec3::new(-0.5, 0.5, -0.5) + offs, Vec3::new(0.6, 0.5, 0.6)),
        PlayerVertex::new(Vec3::new(0.5, 0.5, -0.5) + offs, Vec3::new(0.4, 0.5, 0.7)),
    ));
    // -z
    mesh.push_quad(Quad::new(
        PlayerVertex::new(Vec3::new(-0.5, -0.5, -0.5) + offs, Vec3::new(0.2, 0.2, 0.4)),
        PlayerVertex::new(Vec3::new(0.5, -0.5, -0.5) + offs, Vec3::new(0.3, 0.3, 0.7)),
        PlayerVertex::new(Vec3::new(0.5, 0.5, -0.5) + offs, Vec3::new(0.4, 0.4, 0.1)),
        PlayerVertex::new(Vec3::new(-0.5, 0.5, -0.5) + offs, Vec3::new(0.5, 0.5, 0.3)),
    ));
    // +z
    mesh.push_quad(Quad::new(
        PlayerVertex::new(Vec3::new(-0.5, 0.5, 0.5) + offs, Vec3::new(0.1, 0.5, 0.1)),
        PlayerVertex::new(Vec3::new(0.5, 0.5, 0.5) + offs, Vec3::new(0.2, 0.8, 0.3)),
        PlayerVertex::new(Vec3::new(0.5, -0.5, 0.5) + offs, Vec3::new(0.3, 0.5, 0.7)),
        PlayerVertex::new(Vec3::new(-0.5, -0.5, 0.5) + offs, Vec3::new(0.4, 0.3, 0.9)),
    ));
    mesh
}
