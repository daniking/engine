pub mod camera;
pub mod cli;
pub mod debug;
pub mod error;
pub mod event;
pub mod input;
pub mod render;
pub mod session;
pub mod singleplayer;
pub mod state;
pub mod terrain;
pub mod ui;
pub mod window;

use std::net::SocketAddr;

use clap::Parser;
use common::{span, time::Time};
use session::SessionState;
use tracing::info;
use window::Window;

use crate::{
    cli::ClientArgs,
    singleplayer::Singleplayer,
    state::{State, StateResult},
    ui::EguiState,
};

pub struct App {
    window: Window,
    egui_state: ui::EguiState,
    time: Time,
    sp: Option<Singleplayer>,
}
fn main() {
    let args = ClientArgs::parse();

    common::setup_tracing();
    info!("Starting client in: {:?} mode", args.mode);
    let req = Window::new();
    let (mut window, event_loop) = match req {
        Ok(t) => t,
        // TODO: print some user friendly message with a possible solution.
        Err(e) => panic!("Could not create window: {:?}", e),
    };

    window.grab_cursor(true);
    let time = Time::new();
    let sp = match args.mode {
        cli::GameMode::Singleplayer => Some(Singleplayer::new()),
        _ => None,
    };
    let addr = match args.mode {
        cli::GameMode::Client => {
            if let Some(addr) = args.addr {
                let server_addr = format!("{}:{}", addr, args.port).parse::<SocketAddr>();
                match server_addr {
                    Ok(addr) => addr,
                    Err(err) => panic!("Failed to parse server address: {:?}", err),
                }
            } else {
                panic!("Server address was not provided");
            }
        }
        cli::GameMode::Singleplayer => {
            let sp = sp.as_ref().expect("Singleplayer mode was not initialized");

            sp.recv()
        }
    };
    let mut session = SessionState::new(args.mode.into(), addr, window.renderer_mut());
    session.init();
    let mut states: Vec<Box<dyn State>> = vec![Box::new(session)];

    tracing::debug!("Started session state");
    let egui_state = EguiState::new(window.window_impl());

    let mut app = App {
        window,
        egui_state,
        time,
        sp,
    };
    //  Add tracing spans
    event_loop.run(move |event, _, control_flow| {
        // Continuously run the event loop, even if the OS hasn't dispatched any events.
        control_flow.set_poll();

        let scale = app.window.scale_factor();

        // Event handling
        match event {
            winit::event::Event::WindowEvent { event, .. } => {
                span!(guard, "Collect WindowEvent");
                let response = app.egui_state.capture_event(&event);
                // Only pass events to the window if they weren't consumed by egui.
                if !response.consumed {
                    app.window.collect_window_event(event);
                }
            }
            winit::event::Event::DeviceEvent { event, .. } => {
                span!(guard, "Collect DeviceEvent");
                app.window.collect_device_event(event);
            }
            winit::event::Event::MainEventsCleared => {
                span!(guard, "Handle Update");
                app.time.tick();

                let Some(state) = states.last_mut() else {
                    control_flow.set_exit();
                    return;
                };
                let events = app.window.take_events();
                match state.tick(&mut app, events) {
                    StateResult::Continue => (),
                    StateResult::Exit => control_flow.set_exit(),
                }
                let binding = state.as_ref().binding();
                if let Some(mut frame) = app
                    .window
                    .renderer_mut()
                    .render(binding)
                    .expect("Unrecoverable render error")
                {
                    state.render(&mut frame);
                    if state.enable_gui() {
                        frame.render_egui(&app.egui_state, scale);
                    }
                };
            }
            _ => (),
        }
    });
}
