use std::{net::SocketAddr, sync::mpsc, thread::JoinHandle};

use common::time::Time;
use server::Server;
use tracing::info;

pub struct Singleplayer {
    _thread: JoinHandle<()>,
    addr_receiver: mpsc::Receiver<SocketAddr>,
}

impl Singleplayer {
    pub fn new() -> Self {
        let (tx, rx) = std::sync::mpsc::channel();
        let thread = std::thread::spawn(move || {
            let addr = SocketAddr::from(([127, 0, 0, 1], 0));
            info!("Starting server on {}", addr);
            let server = Server::new(addr);
            match server {
                Ok(server) => {
                    let _ = tx.send(server.addr());
                    run_server(server);
                    info!("Server thread has ended");
                }
                Err(err) => panic!("Failed to start server: {:?}", err),
            }
        });

        Self {
            addr_receiver: rx,
            _thread: thread,
        }
    }

    pub fn recv(&self) -> SocketAddr {
        self.addr_receiver.recv().unwrap()
    }
}

fn run_server(mut server: Server) {
    let mut time = Time::new();
    loop {
        time.tick();
        server.tick(time.dt());
    }
}
