use crate::render::error::RenderError;

/// Represents any error that might occur in the actual client engine.
#[derive(Debug)]
pub enum Error {
    Client,
    Server,
    Render(RenderError),
}

impl From<RenderError> for Error {
    fn from(e: RenderError) -> Self {
        Error::Render(e)
    }
}
