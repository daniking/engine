use wgpu::TextureView;

use crate::{
    render::{
        buffer::Buffer,
        device::Device,
        globals::GlobalsBindGroup,
        pipelines::{player::PlayerVertex, terrain::TerrainVertex, Pipelines},
    },
    ui::EguiState,
};

use super::{OtherViews, Renderer};

pub struct Pass<'pass> {
    render_pass: wgpu::RenderPass<'pass>,
    pipelines: &'pass Pipelines,
    renderer: &'pass RendererBorrow<'pass>,
    globals: &'pass GlobalsBindGroup,
}

impl<'pass> Pass<'pass> {
    /// Render a terrain mesh.
    pub fn render_terrain(&mut self, buffer: &'pass Buffer<TerrainVertex>, wireframe: bool) {
        if wireframe {
            self.render_pass
                .set_pipeline(&self.pipelines.terrain_wireframe);
        } else {
            self.render_pass.set_pipeline(&self.pipelines.terrain);
        }
        self.render_pass
            .set_bind_group(0, &self.globals.binding, &[]);
        self.render_pass.set_index_buffer(
            self.renderer.global_index_buffer_u32.slice(),
            wgpu::IndexFormat::Uint32,
        );
        self.render_pass.set_vertex_buffer(0, buffer.buf.slice(..));
        self.render_pass
            .draw_indexed(0..self.renderer.global_index_buffer_u32.len(), 0, 0..1);
    }

    pub fn render_player(
        &mut self,
        index_buffer: &'pass Buffer<u16>,
        buffer: &'pass Buffer<PlayerVertex>,
    ) {
        self.render_pass.set_pipeline(&self.pipelines.player);
        self.render_pass
            .set_bind_group(0, &self.globals.binding, &[]);

        self.render_pass
            .set_index_buffer(index_buffer.slice(), wgpu::IndexFormat::Uint16);
        self.render_pass.set_vertex_buffer(0, buffer.buf.slice(..));
        self.render_pass
            .draw_indexed(0..index_buffer.len(), 0, 0..1);
    }
}

pub struct RendererBorrow<'frame> {
    queue: &'frame wgpu::Queue,
    device: &'frame Device,
    egui_renderer: &'frame mut egui_wgpu::Renderer,
    config: &'frame wgpu::SurfaceConfiguration,
    views: &'frame OtherViews,
    pipelines: &'frame Pipelines,
    global_index_buffer_u32: &'frame Buffer<u32>,
}

pub struct RenderFrame<'frame> {
    encoder: Option<wgpu::CommandEncoder>,
    globals: &'frame GlobalsBindGroup,
    surface_texture: Option<wgpu::SurfaceTexture>,
    renderer: RendererBorrow<'frame>,
    view: TextureView,
}

impl<'frame> RenderFrame<'frame> {
    pub fn new(
        renderer: &'frame mut Renderer,
        encoder: wgpu::CommandEncoder,
        surface_texture: wgpu::SurfaceTexture,
        view: TextureView,
        globals: &'frame GlobalsBindGroup,
    ) -> Self {
        let borrow = RendererBorrow {
            queue: &renderer.queue,
            device: &renderer.device,
            egui_renderer: &mut renderer.egui_renderer,
            views: &renderer.views,
            pipelines: &renderer.pipelines,
            global_index_buffer_u32: &renderer.global_index_buffer_u32,
            config: &renderer.config,
        };
        Self {
            encoder: Some(encoder),
            globals,
            renderer: borrow,
            view,
            surface_texture: Some(surface_texture),
        }
    }

    pub fn make_pass(&mut self) -> Pass<'_> {
        let encoder = self.encoder.as_mut().unwrap();
        let depth = &self.renderer.views.depth;

        let descriptor = &wgpu::RenderPassDescriptor {
            label: Some("First pass"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &self.view,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color {
                        r: 0.1,
                        g: 0.3,
                        b: 0.5,
                        a: 1.0,
                    }),
                    store: true,
                },
                resolve_target: None,
            })],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: depth,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(1.0),
                    store: true,
                }),
                stencil_ops: None,
            }),
        };
        let render_pass = encoder.begin_render_pass(descriptor);
        Pass {
            render_pass,
            pipelines: self.renderer.pipelines,
            renderer: &self.renderer,
            globals: self.globals,
        }
    }

    pub fn render_egui(&mut self, state: &EguiState, scale_factor: f32) {
        let output = state.ctx.end_frame();
        let texture_delta = output.textures_delta;
        let paint_jobs = state.ctx.tessellate(output.shapes);

        for (id, dt) in texture_delta.set {
            self.renderer.egui_renderer.update_texture(
                self.renderer.device.handle(),
                self.renderer.queue,
                id,
                &dt,
            );
        }

        let screen_descriptor = egui_wgpu::renderer::ScreenDescriptor {
            size_in_pixels: [self.renderer.config.width, self.renderer.config.height],
            pixels_per_point: scale_factor,
        };
        self.renderer.egui_renderer.update_buffers(
            self.renderer.device.handle(),
            self.renderer.queue,
            self.encoder.as_mut().unwrap(),
            &paint_jobs,
            &screen_descriptor,
        );
        let mut render_pass =
            self.encoder
                .as_mut()
                .unwrap()
                .begin_render_pass(&wgpu::RenderPassDescriptor {
                    label: None,
                    color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                        view: &self.view,
                        resolve_target: None,
                        ops: wgpu::Operations {
                            load: wgpu::LoadOp::Load,
                            store: true,
                        },
                    })],
                    depth_stencil_attachment: None,
                });

        self.renderer
            .egui_renderer
            .render(&mut render_pass, &paint_jobs, &screen_descriptor);

        drop(render_pass);

        for id in texture_delta.free {
            self.renderer.egui_renderer.free_texture(&id);
        }
    }
}

impl Drop for RenderFrame<'_> {
    fn drop(&mut self) {
        let encoder = self.encoder.take().unwrap();
        self.renderer
            .queue
            .submit(std::iter::once(encoder.finish()));
        self.surface_texture.take().unwrap().present();
    }
}
