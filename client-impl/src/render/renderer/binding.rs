use crate::render::{
    buffer::GlobalBuffer,
    globals::{Globals, GlobalsBindGroup},
};

use super::Renderer;

impl Renderer {
    pub fn bind_globals(&self, globals: &GlobalBuffer<Globals>) -> GlobalsBindGroup {
        self.layout
            .bind_globals(&self.device, globals, &self.atlas.texture)
    }
}
