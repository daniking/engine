pub mod binding;
pub mod frame;

use common::math::Vec2;
use pollster::FutureExt;
use tracing::{info, warn};
use wgpu::SurfaceConfiguration;

use crate::render::pipelines::player::PlayerPipeline;

use self::frame::RenderFrame;

use super::{
    atlas::Atlas,
    buffer::{Buffer, GlobalBuffer},
    device::Device,
    error::RenderError,
    globals::{GlobalsBindGroup, GlobalsBindGroupLayout},
    mesh::QUAD_INDICES_PATTERN,
    pipelines::{terrain::TerrainPipeline, Pipelines},
    texture::Texture,
    Vertex,
};

struct OtherViews {
    depth: wgpu::TextureView,
}
pub struct Renderer {
    surface: wgpu::Surface,
    queue: wgpu::Queue,
    pub device: Device, // TODO: make this private
    config: wgpu::SurfaceConfiguration,
    global_index_buffer_u32: Buffer<u32>,
    layout: GlobalsBindGroupLayout,
    dimensions: Vec2<u32>,
    egui_renderer: egui_wgpu::Renderer,
    /// Render texture views
    views: OtherViews,
    pipelines: Pipelines,
    atlas: Atlas,
}

impl Renderer {
    pub fn new(window: &winit::window::Window) -> Result<Self, RenderError> {
        // Select backend based on env var
        let backends = std::env::var("WGPU_BACKEND")
            .ok()
            .and_then(|env| match env.to_lowercase().as_str() {
                "vulkan" => Some(wgpu::Backends::VULKAN),
                "metal" => Some(wgpu::Backends::METAL),
                "dx12" => Some(wgpu::Backends::DX12),
                "dx11" => Some(wgpu::Backends::DX11),
                "opengl" => Some(wgpu::Backends::GL),
                "primary" => Some(wgpu::Backends::PRIMARY),
                "secondary" => Some(wgpu::Backends::SECONDARY),
                "all" => Some(wgpu::Backends::all()),
                _ => None,
            })
            .unwrap_or(wgpu::Backends::PRIMARY);

        info!("Using {:?} backend", backends);

        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends,
            dx12_shader_compiler: wgpu::Dx12Compiler::default(),
        });

        let surface = unsafe { instance.create_surface(&window) }?;

        // TODO: Select adapter based on env var
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .block_on()
            .ok_or(RenderError::AdapterNotFound)?;

        info!("Using adapter: {:?}", adapter.get_info());

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty() | wgpu::Features::POLYGON_MODE_LINE,
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None, // Trace path
            )
            .block_on()?;
        let device = Device::new(device);

        let size = window.inner_size();

        let sfc_caps = surface.get_capabilities(&adapter);

        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: sfc_caps.formats[0],
            width: size.width,
            height: size.height,
            present_mode: sfc_caps.present_modes[0],
            alpha_mode: sfc_caps.alpha_modes[0],
            view_formats: vec![],
        };

        surface.configure(device.handle(), &config);

        let layout = GlobalsBindGroupLayout::new(&device);
        let terrain_pipeline =
            TerrainPipeline::new(&device, &config, &[&layout.binding_layout], false);
        let terrain_wireframe_pipeline =
            TerrainPipeline::new(&device, &config, &[&layout.binding_layout], true);

        let global_index_buffer_u32 = compute_cube_index_buf_u32(device.handle(), 5000);

        let egui_renderer = egui_wgpu::Renderer::new(
            device.handle(),
            wgpu::TextureFormat::Bgra8UnormSrgb,
            None,
            1,
        );
        let views = Self::create_views(&config, device.handle());
        let pipelines = Pipelines {
            terrain: terrain_pipeline,
            terrain_wireframe: terrain_wireframe_pipeline,
            player: PlayerPipeline::new(&device, &config, &[&layout.binding_layout], false),
        };
        let atlas = Atlas::new(
            include_bytes!("../../../../assets/atlas.png"),
            &device,
            &queue,
        );
        let this = Self {
            surface,
            device,
            queue,
            dimensions: Vec2::new(size.width, size.height),
            global_index_buffer_u32,
            config,
            pipelines,
            layout,
            egui_renderer,
            views,
            atlas,
        };

        Ok(this)
    }

    pub fn render<'frame>(
        &'frame mut self,
        binding: &'frame GlobalsBindGroup,
    ) -> Result<Option<RenderFrame>, RenderError> {
        let tex = match self.surface.get_current_texture() {
            Ok(frame) => frame,

            Err(wgpu::SurfaceError::Timeout) => {
                return Ok(None);
            }
            Err(error @ wgpu::SurfaceError::Lost) => {
                warn!("Reconfiguring surface. {}", error);
                // If the swapchain is lost, reconfigure it
                self.resize(self.dimensions);
                return Ok(None);
            }
            Err(err @ wgpu::SurfaceError::Outdated) => {
                warn!("Surface Outdated. {}", err);
                return Ok(None);
            }
            // Out of memory is unrecoverable.
            Err(wgpu::SurfaceError::OutOfMemory) => return Err(RenderError::OutOfMemory),
        };
        let view = tex
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());
        let encoder = self.device.create_command_encoder();
        Ok(Some(RenderFrame::new(self, encoder, tex, view, binding)))
    }

    pub fn resize(&mut self, dimensions: Vec2<u32>) {
        if dimensions.x == 0 || dimensions.y == 0 {
            // Resize with 0 width and height is used by winit to signal a minimize event on Windows.
            // Refer to: https://github.com/rust-windowing/winit/issues/208
            // This solves an issue where the app would panic when minimizing on Windows.
            return;
        }
        self.config.width = dimensions.x;
        self.config.height = dimensions.y;
        self.surface.configure(self.device.handle(), &self.config);
        self.views = Self::create_views(&self.config, self.device.handle());
    }

    pub fn create_global_buffer<T: Copy + bytemuck::Pod>(&self, data: &[T]) -> GlobalBuffer<T> {
        GlobalBuffer::new(&self.device, data)
    }

    pub fn create_vertex_buffer<V: Vertex>(&mut self, data: &[V]) -> Buffer<V> {
        self.check_index_length::<V>(data.len());
        self.device.create_vertex_buffer(data)
    }

    pub fn update_binding_buffer<T: Copy + bytemuck::Pod>(&self, buf: &Buffer<T>, data: &[T]) {
        buf.update(&self.queue, data)
    }

    pub fn dimensions(&self) -> Vec2<u32> {
        self.dimensions
    }

    pub fn check_index_length<V: Vertex>(&mut self, len: usize) {
        let l = len / 6 * 4;
        match V::INDEX_BUFFER_FORMAT {
            Some(wgpu::IndexFormat::Uint16) => {
                // TODO: implement this
            }
            Some(wgpu::IndexFormat::Uint32) => {
                if self.global_index_buffer_u32.len() > l as u32 {
                    return;
                }
                if len > u32::MAX as usize {
                    panic!(
                        "Too many vertices for {} using u32 index buffer. Count: {}",
                        core::any::type_name::<V>(),
                        len
                    );
                }
                info!(
                    "Recreating index buffer for {}, with {} vertices",
                    core::any::type_name::<V>(),
                    len
                );
                self.global_index_buffer_u32 =
                    compute_cube_index_buf_u32(self.device.handle(), len);
            }
            None => {
                info!("GOT NO INDEX BUFFER");
            }
        }
    }

    fn create_views(config: &SurfaceConfiguration, device: &wgpu::Device) -> OtherViews {
        let dims = Vec2::<u32>::from((config.width, config.height));
        let depth_texure = Texture::depth(device, &dims);
        OtherViews {
            depth: depth_texure.view,
        }
    }
}

pub fn compute_cube_index_buf_u32(device: &wgpu::Device, vertex_count: usize) -> Buffer<u32> {
    let indices = compute_mesh_indices_u32(vertex_count);
    Buffer::new(device, wgpu::BufferUsages::INDEX, &indices)
}

pub fn compute_cube_index_buf_u16(device: &wgpu::Device, vertex_count: usize) -> Buffer<u16> {
    let indices = compute_mesh_indices_u16(vertex_count);
    Buffer::new(device, wgpu::BufferUsages::INDEX, &indices)
}

fn compute_mesh_indices_u32(vertex_count: usize) -> Vec<u32> {
    assert!(vertex_count <= u32::MAX as usize);
    let indices = QUAD_INDICES_PATTERN
        .iter()
        .cycle()
        .copied()
        .take(vertex_count / 4 * 6)
        .enumerate()
        .map(|(i, j)| (i / 6 * 4 + j as usize) as u32)
        .collect::<Vec<_>>();
    indices
}

fn compute_mesh_indices_u16(vertex_count: usize) -> Vec<u16> {
    assert!(vertex_count <= u16::MAX as usize);
    let indices = QUAD_INDICES_PATTERN
        .iter()
        .cycle()
        .copied()
        .take(vertex_count / 4 * 6)
        .enumerate()
        .map(|(i, j)| (i / 6 * 4 + j as usize) as u16)
        .collect::<Vec<_>>();
    indices
}
