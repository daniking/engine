use crate::render::buffer::Buffer;

/// A wrapper around [wgpu::Device] for convenience.
///
/// It provides a few helper methods for creating common resources
/// such as [Buffer], [wgpu::ShaderModule], and [wgpu::RenderPipeline].
///
/// Additionally, it sets up an error handler for uncaptured device errors.
pub struct Device {
    /// The underlying [wgpu::Device] handle.
    handle: wgpu::Device,
}
impl Device {
    /// Creates a new [Device] for the given [wgpu::Device].
    ///
    /// The uncaptured error handler is also set up.
    pub fn new(device: wgpu::Device) -> Self {
        // device.on_uncaptured_error(Box::new(|e| {
        //     tracing::error!("Uncaptured device error: {:?}", e);
        // }));
        Self { handle: device }
    }

    /// Creates a new [Buffer] for the given usage and data.
    pub fn create_buffer<T>(&self, usage: wgpu::BufferUsages, data: &[T]) -> Buffer<T>
    where
        T: Copy + bytemuck::Pod,
    {
        Buffer::new(&self.handle, usage, data)
    }

    /// Helper method for creating a [Buffer] with the `wgpu::BufferUsages::VERTEX` usage.
    pub fn create_vertex_buffer<T>(&self, data: &[T]) -> Buffer<T>
    where
        T: Copy + bytemuck::Pod,
    {
        Self::create_buffer(self, wgpu::BufferUsages::VERTEX, data)
    }

    pub fn create_quad_index_buffer<T>(&self, _data: &[T]) {}

    /// Creates a new [wgpu::ShaderModule].
    ///
    /// The descriptor is needed to specify the shader source.
    ///
    /// - Tip: Use the `wgpu::include_wgsl!()` to create the shader descriptor.
    pub fn create_shader_module(&self, desc: wgpu::ShaderModuleDescriptor) -> wgpu::ShaderModule {
        self.handle.create_shader_module(desc)
    }

    /// Creates a new [wgpu::RenderPipeline].
    ///
    /// This is a quite verbose process but it's being abstracted away here.
    pub fn create_render_pipeline(
        &self,
        name: &str,
        shader: wgpu::ShaderModule,
        uniforms: &[&wgpu::BindGroupLayout],
        sfc_config: &wgpu::SurfaceConfiguration,
        vertex_buffers: &[wgpu::VertexBufferLayout],
        depth_stencil: Option<wgpu::DepthStencilState>,
        mode: wgpu::PolygonMode,
        render_backface: bool,
    ) -> wgpu::RenderPipeline {
        // Constants
        const VERTEX_SHADER_ENTRY_POINT: &str = "vs_main";
        const FRAGMENT_SHADER_ENTRY_POINT: &str = "fs_main";
        // These are use for debugging purposes
        let pipeline_layout = format!("{} Pipeline Layout", name);
        let pipeline_label = format!("{} Pipeline", name);

        let layout_descriptor = &wgpu::PipelineLayoutDescriptor {
            label: Some(&pipeline_layout),
            bind_group_layouts: uniforms,
            push_constant_ranges: &[],
        };
        let layout = self.handle.create_pipeline_layout(layout_descriptor);
        let binding = [Some(wgpu::ColorTargetState {
            format: sfc_config.format,
            blend: Some(wgpu::BlendState::REPLACE),
            write_mask: wgpu::ColorWrites::ALL,
        })];
        let pipeline_descriptor = &wgpu::RenderPipelineDescriptor {
            label: Some(&pipeline_label),
            layout: Some(&layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: VERTEX_SHADER_ENTRY_POINT,
                buffers: vertex_buffers,
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: FRAGMENT_SHADER_ENTRY_POINT,
                targets: &binding,
            }),
            // TODO: support wireframe mode
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: if render_backface {
                    None
                } else {
                    Some(wgpu::Face::Back)
                },
                polygon_mode: mode,
                conservative: false,
                unclipped_depth: false,
            },
            // TODO: support depth buffer
            depth_stencil,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        };
        self.handle.create_render_pipeline(pipeline_descriptor)
    }

    /// Creates a [wgpu::CommandEncoder].
    ///
    /// It is used to encode GPU commands.
    pub fn create_command_encoder(&self) -> wgpu::CommandEncoder {
        let descriptor = &wgpu::CommandEncoderDescriptor {
            label: Some("Command Encoder"),
        };
        self.handle.create_command_encoder(descriptor)
    }

    pub fn create_bind_group_layout(
        &self,
        entries: &[wgpu::BindGroupLayoutEntry],
    ) -> wgpu::BindGroupLayout {
        let descriptor = &wgpu::BindGroupLayoutDescriptor {
            label: None,
            entries,
        };
        self.handle.create_bind_group_layout(descriptor)
    }

    pub fn create_bind_group(
        &self,
        layout: &wgpu::BindGroupLayout,
        entries: &[wgpu::BindGroupEntry],
    ) -> wgpu::BindGroup {
        let descriptor = &wgpu::BindGroupDescriptor {
            label: None,
            layout,
            entries,
        };
        self.handle.create_bind_group(descriptor)
    }

    /// Returns an immutable reference to the underlying [wgpu::Device].
    pub fn handle(&self) -> &wgpu::Device {
        &self.handle
    }
}
