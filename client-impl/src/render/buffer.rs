use wgpu::util::DeviceExt;

use super::device::Device;

/// Represents a GPU buffer.
///
/// It is a wrapper around [wgpu::Buffer].
/// This specific implementation allows to create and initialize a buffer in one step.
/// However, it does not allow to modify the buffer after creation.
pub struct Buffer<T: Copy + bytemuck::Pod> {
    /// The underlying buffer handle.
    pub(super) buf: wgpu::Buffer,
    /// The length of the buffer.
    len: usize,
    /// A phantom data field to make the compiler happy.
    ///
    /// It is needed because the generic type `T` is not used in the struct.
    /// However, it is very helpful to have a generic buffer type.
    phantom: std::marker::PhantomData<T>,
}

impl<T: Copy + bytemuck::Pod> Buffer<T> {
    /// Creates a new [Buffer].
    ///
    /// The buffer is initialized with the given data.
    pub fn new(device: &wgpu::Device, usage: wgpu::BufferUsages, data: &[T]) -> Self {
        let descriptor = wgpu::util::BufferInitDescriptor {
            label: None,
            contents: bytemuck::cast_slice(data),
            usage,
        };

        Self {
            buf: device.create_buffer_init(&descriptor),
            phantom: std::marker::PhantomData,
            len: data.len(),
        }
    }

    pub fn update(&self, queue: &wgpu::Queue, data: &[T]) {
        if data.is_empty() {
            return;
        }
        queue.write_buffer(&self.buf, 0, bytemuck::cast_slice(data))
    }

    pub fn slice(&self) -> wgpu::BufferSlice<'_> {
        self.buf.slice(..)
    }

    pub fn binding(&self) -> wgpu::BindingResource {
        self.buf.as_entire_binding()
    }

    /// Gives you the length of the buffer.
    pub fn len(&self) -> u32 {
        self.len as u32
    }
}

/// This is a wrapper around [Buffer].
///
/// It is used to store shader globals.
pub struct GlobalBuffer<T: Copy + bytemuck::Pod> {
    buf: Buffer<T>,
}

impl<T: Copy + bytemuck::Pod> GlobalBuffer<T> {
    /// Creates a new [GlobalBuffer].
    pub fn new(device: &Device, data: &[T]) -> Self {
        Self {
            buf: device.create_buffer(
                wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                data,
            ),
        }
    }

    /// Updates the buffer with the given data.
    pub fn update(&self, queue: &wgpu::Queue, data: &[T]) {
        self.buf.update(queue, data);
    }
}

impl<T: Copy + bytemuck::Pod> std::ops::Deref for GlobalBuffer<T> {
    type Target = Buffer<T>;

    fn deref(&self) -> &Self::Target {
        &self.buf
    }
}
