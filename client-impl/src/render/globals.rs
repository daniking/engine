use common::math::Mat4;

use super::{buffer::GlobalBuffer, device::Device, texture::Texture};

/// The raw global data that is available to all shaders.
///
/// It derives `bytemuck::Pod` and `bytemuck::Zeroable` so that it can be used as a [super::buffer::Buffer].
#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Globals {
    view_mat: [[f32; 4]; 4],
    proj_mat: [[f32; 4]; 4],
}

impl Default for Globals {
    fn default() -> Self {
        Self {
            view_mat: Mat4::identity().into_col_arrays(),
            proj_mat: Mat4::identity().into_col_arrays(),
        }
    }
}

impl Globals {
    pub fn new(view_mat: Mat4<f32>, proj_mat: Mat4<f32>) -> Self {
        Self {
            view_mat: view_mat.into_col_arrays(),
            proj_mat: proj_mat.into_col_arrays(),
        }
    }
}

/// This is the global bind group that is attached to all
/// pipelines that require access to the global data.
pub struct GlobalsBindGroup {
    pub binding: wgpu::BindGroup,
}

/// Describes the layout of the global bind group.
///
/// Once created, this layout can be used to create a [GlobalsBindGroup],
/// by calling `[GlobalsBindGroupLayout::bind_globals]`.
pub struct GlobalsBindGroupLayout {
    pub binding_layout: wgpu::BindGroupLayout,
}

impl GlobalsBindGroup {
    /// Returns the bind group entries for the global bind group.
    pub fn binding_entries<'a>(
        buffer: &'a GlobalBuffer<Globals>,
        test_texture: &'a Texture,
    ) -> Vec<wgpu::BindGroupEntry<'a>> {
        vec![
            wgpu::BindGroupEntry {
                binding: 0,
                resource: buffer.binding(),
            },
            wgpu::BindGroupEntry {
                binding: 1,
                resource: wgpu::BindingResource::TextureView(&test_texture.view),
            },
            wgpu::BindGroupEntry {
                binding: 2,
                resource: wgpu::BindingResource::Sampler(&test_texture.sampler),
            },
        ]
    }
}

impl GlobalsBindGroupLayout {
    /// Creates a new global bind group layout.
    pub fn new(device: &Device) -> Self {
        let global_layout = device.create_bind_group_layout(&Self::layout_entries());
        Self {
            binding_layout: global_layout,
        }
    }

    /// Creates a new global bind group.
    ///
    /// This bind group can be attached to any pipeline that requires access to the global data.
    pub fn bind_globals(
        &self,
        device: &Device,
        buffer: &GlobalBuffer<Globals>,
        atlas: &Texture,
    ) -> GlobalsBindGroup {
        let binding = device.create_bind_group(
            &self.binding_layout,
            &GlobalsBindGroup::binding_entries(buffer, atlas),
        );

        GlobalsBindGroup { binding }
    }

    /// Returns the bind group layout entries for the global bind group.
    pub fn layout_entries() -> Vec<wgpu::BindGroupLayoutEntry> {
        vec![
            wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::VERTEX,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                },
                count: None,
            },
            // // Test texture
            wgpu::BindGroupLayoutEntry {
                binding: 1,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    multisampled: false,
                    view_dimension: wgpu::TextureViewDimension::D2,
                    sample_type: wgpu::TextureSampleType::Float { filterable: true },
                },
                count: None,
            },
            // Test sampler
            wgpu::BindGroupLayoutEntry {
                binding: 2,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                count: None,
            },
        ]
    }
}
