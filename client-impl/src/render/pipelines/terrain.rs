use common::{
    math::{Vec2, Vec3},
    world::block::BlockId,
};

use crate::render::{atlas::block_tex_map, device::Device, texture::Texture, Vertex};

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Zeroable, bytemuck::Pod)]
pub struct TerrainVertex {
    pub pos: [f32; 3],
    pub uv: [f32; 2],
    pub norm: [f32; 3],
}

impl TerrainVertex {
    pub fn new(pos: Vec3<f32>, block: BlockId, norm: Vec3<f32>, texture_pos: Vec2<u8>) -> Self {
        Self {
            pos: pos.into_array(),
            uv: block_tex_map(norm.into(), block, texture_pos.into_array()),
            norm: norm.into_array(),
        }
    }
}

impl Vertex for TerrainVertex {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        const ATTRS: &[wgpu::VertexAttribute; 3] =
            &wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x2, 2 => Float32x3];
        wgpu::VertexBufferLayout {
            array_stride: Self::STRIDE,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: ATTRS,
        }
    }
    const INDEX_BUFFER_FORMAT: Option<wgpu::IndexFormat> = Some(wgpu::IndexFormat::Uint32);
}

// TODO: document this
pub struct TerrainPipeline(pub wgpu::RenderPipeline);

impl TerrainPipeline {
    pub fn new(
        device: &Device,
        sfc_cfg: &wgpu::SurfaceConfiguration,
        uniforms: &[&wgpu::BindGroupLayout],
        wireframe: bool,
    ) -> Self {
        let shader_desc = wgpu::include_wgsl!("../../../../assets/shaders/terrain.wgsl");
        let pipeline = device.create_render_pipeline(
            "Terrain",
            device.create_shader_module(shader_desc),
            uniforms,
            sfc_cfg,
            &[TerrainVertex::desc()],
            Some(wgpu::DepthStencilState {
                format: Texture::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: wgpu::StencilState::default(),
                bias: wgpu::DepthBiasState::default(),
            }),
            if wireframe {
                wgpu::PolygonMode::Line
            } else {
                wgpu::PolygonMode::Fill
            },
            false,
        );
        Self(pipeline)
    }
}

impl std::ops::Deref for TerrainPipeline {
    type Target = wgpu::RenderPipeline;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
