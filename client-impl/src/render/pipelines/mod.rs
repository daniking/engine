pub mod player;
pub mod terrain;

/// A collection of all the pipelines used by the renderer.
pub struct Pipelines {
    pub terrain: terrain::TerrainPipeline,
    pub terrain_wireframe: terrain::TerrainPipeline,
    pub player: player::PlayerPipeline,
}
