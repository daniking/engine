use common::math::Vec3;

use crate::render::{device::Device, texture::Texture, Vertex};

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Zeroable, bytemuck::Pod)]
pub struct PlayerVertex {
    pub pos: [f32; 3],
    pub color: [f32; 3],
}

impl PlayerVertex {
    pub fn new(pos: Vec3<f32>, color: Vec3<f32>) -> Self {
        Self {
            pos: pos.into_array(),
            color: color.into_array(),
        }
    }
}

impl Vertex for PlayerVertex {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        const ATTRS: &[wgpu::VertexAttribute; 2] =
            &wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x2];
        wgpu::VertexBufferLayout {
            array_stride: Self::STRIDE,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: ATTRS,
        }
    }
    const INDEX_BUFFER_FORMAT: Option<wgpu::IndexFormat> = Some(wgpu::IndexFormat::Uint16);
}

// TODO: document this
pub struct PlayerPipeline(pub wgpu::RenderPipeline);

impl PlayerPipeline {
    pub fn new(
        device: &Device,
        sfc_cfg: &wgpu::SurfaceConfiguration,
        uniforms: &[&wgpu::BindGroupLayout],
        wireframe: bool,
    ) -> Self {
        let shader_desc = wgpu::include_wgsl!("../../../../assets/shaders/player.wgsl");
        let pipeline = device.create_render_pipeline(
            "Player",
            device.create_shader_module(shader_desc),
            uniforms,
            sfc_cfg,
            &[PlayerVertex::desc()],
            Some(wgpu::DepthStencilState {
                format: Texture::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: wgpu::StencilState::default(),
                bias: wgpu::DepthBiasState::default(),
            }),
            if wireframe {
                wgpu::PolygonMode::Line
            } else {
                wgpu::PolygonMode::Fill
            },
            true,
        );
        Self(pipeline)
    }
}

impl std::ops::Deref for PlayerPipeline {
    type Target = wgpu::RenderPipeline;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
