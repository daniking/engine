use common::{math::Vec3, world::block::BlockId};

use super::{device::Device, texture::Texture};

#[derive(Debug, Clone, Copy)]
pub enum TextureId {
    Dirt,
    Grass,
    GrassFull,
    Stone,
}

const ATLAS_SIZE: f32 = 256.0;
const TEXTURE_SIZE: f32 = 16.0;

#[derive(Debug, Clone, Copy)]
pub enum Dir {
    Up,
    Down,
    Left,
    Right,
    Back,
    Front,
}

impl Dir {
    pub fn into_vec(&self) -> Vec3<i32> {
        match self {
            Dir::Up => Vec3::unit_y(),
            Dir::Down => -Vec3::unit_y(),
            Dir::Left => -Vec3::unit_x(),
            Dir::Right => Vec3::unit_x(),
            Dir::Back => -Vec3::unit_z(),
            Dir::Front => Vec3::unit_z(),
        }
    }
}

impl From<Vec3<f32>> for Dir {
    fn from(value: Vec3<f32>) -> Self {
        if value == Vec3::unit_y() {
            Dir::Up
        } else if value == -Vec3::unit_y() {
            Dir::Down
        } else if value == -Vec3::unit_x() {
            Dir::Left
        } else if value == Vec3::unit_x() {
            Dir::Right
        } else if value == -Vec3::unit_z() {
            Dir::Back
        } else if value == Vec3::unit_z() {
            Dir::Front
        } else {
            panic!("Invalid direction vector")
        }
    }
}

pub fn atlas_uv_map(texture_id: TextureId, x: u8, y: u8) -> [f32; 2] {
    let mut offset_x = (texture_id as u8 % 16) as f32 * TEXTURE_SIZE;
    let mut offset_y = (texture_id as u8 / 16) as f32 * TEXTURE_SIZE;

    if x == 1 {
        offset_x += 16.0;
    }

    if y == 1 {
        offset_y += 16.0;
    }

    [offset_x / ATLAS_SIZE, offset_y / ATLAS_SIZE]
}

pub fn block_tex_map(dir: Dir, id: BlockId, pos: [u8; 2]) -> [f32; 2] {
    let texture_id = match id {
        BlockId::Dirt => TextureId::Dirt,
        BlockId::Grass => TextureId::Grass,
        BlockId::GrassTop => match dir {
            Dir::Up => TextureId::GrassFull,
            Dir::Down => TextureId::Dirt,
            Dir::Left => TextureId::Grass,
            Dir::Right => TextureId::Grass,
            Dir::Back => TextureId::Grass,
            Dir::Front => TextureId::Grass,
        },
        BlockId::Stone => TextureId::Stone,
        _ => panic!("Invalid block id"),
    };
    atlas_uv_map(texture_id, pos[0], pos[1])
}

pub struct Atlas {
    pub texture: Texture,
}

impl Atlas {
    pub fn new(buf: &[u8], device: &Device, queue: &wgpu::Queue) -> Self {
        let image = image::load_from_memory(buf).expect("Failed to load image");
        let texture = Texture::new(device, queue, image);
        Self { texture }
    }
}
