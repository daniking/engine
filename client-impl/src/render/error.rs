/// Errors that can occur on the render system.
#[derive(Debug)]
pub enum RenderError {
    DeviceRequest(wgpu::RequestDeviceError),
    AdapterNotFound,
    CreateSurface(wgpu::CreateSurfaceError), // TODO: Add more errors
    OutOfMemory,
}

impl From<wgpu::CreateSurfaceError> for RenderError {
    fn from(e: wgpu::CreateSurfaceError) -> Self {
        RenderError::CreateSurface(e)
    }
}

impl From<wgpu::RequestDeviceError> for RenderError {
    fn from(e: wgpu::RequestDeviceError) -> Self {
        RenderError::DeviceRequest(e)
    }
}
