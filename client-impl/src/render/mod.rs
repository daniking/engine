pub mod atlas;
pub mod buffer;
pub mod device;
pub mod error;
pub mod globals;
pub mod mesh;
pub mod pipelines;
pub mod renderer;
pub mod texture;

/// Represents a Vertex.
///
/// Provides a common interface for vertex types.
pub trait Vertex: Copy + bytemuck::Pod {
    /// The stride of the vertex.
    const STRIDE: wgpu::BufferAddress = std::mem::size_of::<Self>() as wgpu::BufferAddress;
    /// This is the format of the index buffer.
    ///
    /// If this is `None`, then the pipeline does not use an index buffer.
    const INDEX_BUFFER_FORMAT: Option<wgpu::IndexFormat>;
    /// Describes the layout of the vertex.
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a>;
}
