use common::math::Vec2;

pub type EventLoop = winit::event_loop::EventLoop<()>;

/// Represents an event that can occur on a [Window].
/// It is a subset of the events defined in [winit::event::WindowEvent].
#[derive(Debug, Clone)]
pub enum WindowEvent {
    /// The window has been requested to close, (e.g. by the user clicking the close button).
    Close,
    /// The window has been resized.
    Resize(Vec2<u32>),
    /// A user input event has been received.
    Input(GameInput, bool),
    /// The cursor has been moved.
    CursorMove(Vec2<f32>),
}

/// Represents an incoming user input event.
#[derive(Debug, Clone)]
pub enum InputEvent {
    /// A keyboard key has been pressed.
    Key(winit::event::VirtualKeyCode),
    /// A mouse button has been pressed.
    Mouse(winit::event::MouseButton),
    /// A scancode has been received.
    ScanCode(u32),
}

#[derive(Debug, Clone)]
pub enum GameInput {
    MoveForward,
    MoveBackward,
    MoveLeft,
    MoveRight,
    Jump,
    Sneak,
    CursorTrapToggle,
    F12,
}
