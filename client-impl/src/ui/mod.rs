use crate::debug::DebugInfo;

pub struct EguiState {
    pub window_handle: egui_winit::State,
    pub ctx: egui::Context,
}

impl EguiState {
    pub fn new(handle: &winit::window::Window) -> Self {
        let window_handle = egui_winit::State::new(handle);
        let ctx = egui::Context::default();
        Self { window_handle, ctx }
    }

    pub fn capture_event(
        &mut self,
        event: &winit::event::WindowEvent,
    ) -> egui_winit::EventResponse {
        self.window_handle.on_event(&self.ctx, event)
    }

    pub fn draw(&mut self, window: &winit::window::Window, info: DebugInfo) {
        let egui_input = self.window_handle.take_egui_input(window);
        self.ctx.begin_frame(egui_input);

        egui::Window::new("Debug Menu")
            .default_size([200.0, 200.0])
            .show(&self.ctx, |ui| {
                ui.label(format!("[GameMode]: {:?}", info.mode));
                ui.label(format!("[Ping]: {}ms", info.ping));
                ui.label(format!("[FPS]: {}", 1.0 / info.framerate));
                ui.separator();
                ui.label(format!("[Pos]: {}", info.camera_pos));
                ui.label(format!("[Orientation]: {}", info.camera_target()));
                ui.separator();
                ui.label(format!("[VertexCount]: {}", info.vertex_count));
            });
    }
}
