use common::{
    math::{Mat4, Vec2, Vec3},
    world::chunk::CHUNK_SIZE,
};

const NEAR_PLANE: f32 = 0.1;
const FAR_PLANE: f32 = 100000.0;

#[derive(Clone, Copy)]
pub struct Matrices {
    pub view: Mat4<f32>,
    pub proj: Mat4<f32>,
}

/// Represents a camera in the 3D world.
pub struct Camera {
    /// The direction were the camera is looking at.
    pub target: Vec3<f32>,
    /// The position of the camera.
    pos: Vec3<f32>,
    /// The field of view of the camera in degrees.
    fov: f32,
    /// The aspect ratio of the camera.
    aspect: f32,
    /// Describes the camera rotation in radians.
    ///
    /// The x represents a yaw and the y represents a pitch.
    pub rotation: Vec2<f32>,
    /// The matrices that are used to transform the world.
    matrices: Matrices,
}

impl Camera {
    pub fn new(aspect: f32) -> Self {
        Self {
            aspect,
            rotation: Vec2::new(-90.0f32.to_radians(), 0.0),
            fov: 70.0,
            target: Vec3::zero(),
            pos: Vec3::new(0.0, CHUNK_SIZE.y as f32 + 1.0, -1.1),
            matrices: Matrices {
                view: Mat4::identity(),
                proj: Mat4::identity(),
            },
        }
    }

    pub fn compute_matrices(&mut self) {
        let proj =
            Mat4::perspective_lh_no(self.fov.to_radians(), self.aspect, NEAR_PLANE, FAR_PLANE);
        let view = Mat4::<f32>::look_at_lh(self.pos, self.pos + self.target, Vec3::unit_y());
        self.matrices = Matrices { view, proj };
    }

    pub fn forward(&self) -> Vec3<f32> {
        Vec3::new(f32::cos(self.rotation.x), 0.0, -f32::sin(self.rotation.x))
    }
    pub fn right(&self) -> Vec3<f32> {
        Vec3::new(f32::sin(self.rotation.x), 0.0, f32::cos(self.rotation.x))
    }

    pub fn rotate(&mut self, dx: f32, dy: f32) {
        self.rotation.x += dx.to_radians();
        self.rotation.y += dy.to_radians();

        // Clamp pitch to set vertical angle limits
        self.rotation.y = self.rotation.y.clamp(
            -std::f32::consts::FRAC_PI_2 + 0.0001,
            std::f32::consts::FRAC_PI_2 - 0.0001,
        );

        let (yaw_sin, yaw_cos) = self.rotation.x.sin_cos();
        let (pitch_sin, pitch_cos) = self.rotation.y.sin_cos();

        // yaw_sin z goes negative for left handed coordinate system
        let rotation = Vec3::new(yaw_cos * pitch_cos, pitch_sin, -yaw_sin * pitch_cos).normalized();
        self.target = rotation;
    }

    pub fn set_pos(&mut self, pos: Vec3<f32>) {
        self.pos = pos;
    }

    pub fn set_aspect_ratio(&mut self, aspect: f32) {
        self.aspect = if aspect.is_normal() { aspect } else { 1.0 };
    }

    pub fn looking_dir(&self) -> Vec3<f32> {
        self.target.normalized()
    }

    pub fn pos(&self) -> Vec3<f32> {
        self.pos.round()
    }

    pub fn matrices(&self) -> Matrices {
        self.matrices
    }

    pub fn rotation_vector(&self) -> Vec3<f32> {
        let yaw = self.rotation.x;
        let pitch = self.rotation.y;

        let (sin_yaw, cos_yaw) = yaw.sin_cos();
        let (sin_pitch, cos_pitch) = pitch.sin_cos();

        let rotation = Vec3::new(-sin_yaw * cos_pitch, sin_pitch, -cos_yaw * cos_pitch);

        rotation.normalized()
    }
}
