use std::collections::HashMap;

use crate::render::{
    buffer::Buffer,
    mesh::{Mesh, Quad},
    pipelines::terrain::TerrainVertex,
    renderer::{frame::Pass, Renderer},
};
use common::{
    math::{Vec2, Vec3},
    world::block::{Block, BlockId},
    world::chunk::{Chunk, ChunkPos},
    Direction,
};
use tracing::info;

pub struct Terrain {
    chunks: HashMap<ChunkPos, Chunk>,
    buffer: Option<Buffer<TerrainVertex>>,
    loaded: bool,
}

impl Terrain {
    pub fn new() -> Self {
        Self {
            chunks: HashMap::new(),
            buffer: None,
            loaded: false,
        }
    }

    pub fn insert_chunk(&mut self, at: ChunkPos, chunk: Chunk) {
        // TODO: use returned value?
        self.chunks.insert(at, chunk);
    }

    pub fn tick(&mut self, renderer: &mut Renderer) {
        let radius = 2;
        if !self.loaded {
            let time = std::time::Instant::now();
            for x in -radius..radius {
                for z in -radius..radius {
                    let pos = ChunkPos::new(x, z);
                    self.insert_chunk(pos, Chunk::with_noise(pos))
                }
            }
            info!(
                "Took {}ms to generate {:?} chunks",
                time.elapsed().as_millis(),
                self.chunks.len()
            );

            let mut vertices: Vec<TerrainVertex> = vec![];
            // This has to be enumerated otherwise some weird bug happens when generating mesh.
            for (_, (pos, chunk)) in self.chunks.iter().enumerate() {
                let mesh = self.generate_mesh(chunk, *pos);
                vertices.extend(mesh.vertices());
            }
            self.buffer = Some(renderer.create_vertex_buffer(&vertices));
            self.loaded = true;
        }
    }

    pub fn render<'a>(&'a self, pass: &mut Pass<'a>, wireframe: bool) {
        if let Some(buf) = &self.buffer {
            pass.render_terrain(buf, wireframe);
        }
    }

    pub fn v_count(&self) -> u32 {
        self.buffer.iter().map(|c| c.len()).sum()
    }

    pub fn neighbor_chunk(&self, pos: ChunkPos, target: Direction) -> Option<&Chunk> {
        match target {
            Direction::Left => self.chunks.get(&ChunkPos::new(pos.x - 1, pos.z)),
            Direction::Right => self.chunks.get(&ChunkPos::new(pos.x + 1, pos.z)),
            Direction::Back => self.chunks.get(&ChunkPos::new(pos.x, pos.z - 1)),
            Direction::Front => self.chunks.get(&ChunkPos::new(pos.x, pos.z + 1)),
            _ => None,
        }
    }

    pub fn generate_mesh(&self, chunk: &Chunk, offset: ChunkPos) -> Mesh<TerrainVertex> {
        let mut mesh = Mesh::new();

        let has_solid_neighbor = |pos: Vec3<i32>, dir: Direction| {
            if pos.x >= 0
                && pos.x < Chunk::size().x
                && pos.y >= 0
                && pos.y < Chunk::size().y
                && pos.z >= 0
                && pos.z < Chunk::size().z
            {
                return !chunk.get(pos).unwrap().id().is_air();
            }

            let Some(neighbor) = self.neighbor_chunk(offset, dir) else {
                return false;
            };
            return !neighbor
                .get(Vec3::new(
                    pos.x.rem_euclid(Chunk::size().x),
                    pos.y,
                    pos.z.rem_euclid(Chunk::size().z),
                ))
                .unwrap_or(&Block::air())
                .id()
                .is_air();
        };

        for origin in chunk.iter_pos() {
            let block = match chunk.get(origin) {
                Ok(block) => block.id(),
                Err(_) => continue,
            };

            if block.is_air() {
                continue;
            }

            let world_x = origin.x + offset.x * Chunk::size().x;
            let world_z = origin.z + offset.z * Chunk::size().z;

            let offs = Vec3::new(world_x, origin.y, world_z);

            for face in Direction::iter() {
                if has_solid_neighbor(origin + face.vec(), face) {
                    continue;
                }
                mesh.push_quad(quad(block, face, offs));
            }
        }
        mesh
    }
}

pub fn quad(block: BlockId, dir: Direction, offs: Vec3<i32>) -> Quad<TerrainVertex> {
    let offs = offs.map(|n| n as f32);
    let neg = 0.0;
    let pos = 1.0;

    let (v1, v2, v3, v4) = match dir {
        Direction::Top => (
            TerrainVertex::new(
                Vec3::new(neg, pos, neg) + offs,
                block,
                Vec3::unit_y(),
                Vec2::new(0, 1),
            ),
            TerrainVertex::new(
                Vec3::new(pos, pos, neg) + offs,
                block,
                Vec3::unit_y(),
                Vec2::new(0, 0),
            ),
            TerrainVertex::new(
                Vec3::new(pos, pos, pos) + offs,
                block,
                Vec3::unit_y(),
                Vec2::new(1, 0),
            ),
            TerrainVertex::new(
                Vec3::new(neg, pos, pos) + offs,
                block,
                Vec3::unit_y(),
                Vec2::new(1, 1),
            ),
        ),
        Direction::Bottom => (
            TerrainVertex::new(
                Vec3::new(neg, neg, pos) + offs,
                block,
                -Vec3::unit_y(),
                Vec2::new(0, 1),
            ),
            TerrainVertex::new(
                Vec3::new(pos, neg, pos) + offs,
                block,
                -Vec3::unit_y(),
                Vec2::new(0, 0),
            ),
            TerrainVertex::new(
                Vec3::new(pos, neg, neg) + offs,
                block,
                -Vec3::unit_y(),
                Vec2::new(1, 0),
            ),
            TerrainVertex::new(
                Vec3::new(neg, neg, neg) + offs,
                block,
                -Vec3::unit_y(),
                Vec2::new(1, 1),
            ),
        ),
        Direction::Left => (
            TerrainVertex::new(
                Vec3::new(neg, pos, neg) + offs,
                block,
                -Vec3::unit_x(),
                Vec2::new(1, 0),
            ),
            TerrainVertex::new(
                Vec3::new(neg, pos, pos) + offs,
                block,
                -Vec3::unit_x(),
                Vec2::new(0, 0),
            ),
            TerrainVertex::new(
                Vec3::new(neg, neg, pos) + offs,
                block,
                -Vec3::unit_x(),
                Vec2::new(0, 1),
            ),
            TerrainVertex::new(
                Vec3::new(neg, neg, neg) + offs,
                block,
                -Vec3::unit_x(),
                Vec2::new(1, 1),
            ),
        ),
        Direction::Right => (
            TerrainVertex::new(
                Vec3::new(pos, pos, pos) + offs,
                block,
                Vec3::unit_x(),
                Vec2::new(1, 0),
            ),
            TerrainVertex::new(
                Vec3::new(pos, pos, neg) + offs,
                block,
                Vec3::unit_x(),
                Vec2::new(0, 0),
            ),
            TerrainVertex::new(
                Vec3::new(pos, neg, neg) + offs,
                block,
                Vec3::unit_x(),
                Vec2::new(0, 1),
            ),
            TerrainVertex::new(
                Vec3::new(pos, neg, pos) + offs,
                block,
                Vec3::unit_x(),
                Vec2::new(1, 1),
            ),
        ),

        Direction::Back => (
            TerrainVertex::new(
                Vec3::new(pos, pos, neg) + offs,
                block,
                -Vec3::unit_z(),
                Vec2::new(1, 0),
            ),
            TerrainVertex::new(
                Vec3::new(neg, pos, neg) + offs,
                block,
                -Vec3::unit_z(),
                Vec2::new(0, 0),
            ),
            TerrainVertex::new(
                Vec3::new(neg, neg, neg) + offs,
                block,
                -Vec3::unit_z(),
                Vec2::new(0, 1),
            ),
            TerrainVertex::new(
                Vec3::new(pos, neg, neg) + offs,
                block,
                -Vec3::unit_z(),
                Vec2::new(1, 1),
            ),
        ),
        Direction::Front => (
            TerrainVertex::new(
                Vec3::new(neg, pos, pos) + offs,
                block,
                Vec3::unit_z(),
                Vec2::new(1, 0),
            ),
            TerrainVertex::new(
                Vec3::new(pos, pos, pos) + offs,
                block,
                Vec3::unit_z(),
                Vec2::new(0, 0),
            ),
            TerrainVertex::new(
                Vec3::new(pos, neg, pos) + offs,
                block,
                Vec3::unit_z(),
                Vec2::new(0, 1),
            ),
            TerrainVertex::new(
                Vec3::new(neg, neg, pos) + offs,
                block,
                Vec3::unit_z(),
                Vec2::new(1, 1),
            ),
        ),
    };
    Quad::new(v1, v2, v3, v4)
}
