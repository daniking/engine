use crate::{
    event::WindowEvent,
    render::{globals::GlobalsBindGroup, renderer::frame::RenderFrame},
    App,
};

/// Represents a state of the application.
///
/// A state is a part of the application that has its own
/// logic and can be switched to and from.
/// TODO: implement stacking.
pub trait State {
    fn init(&mut self);

    fn tick(&mut self, app: &mut App, events: Vec<WindowEvent>) -> StateResult;

    fn render(&self, frame: &mut RenderFrame);

    fn binding(&self) -> &GlobalsBindGroup;

    fn enable_gui(&self) -> bool;
}

/// Represents the result of a state tick.
pub enum StateResult {
    Continue,
    Exit,
}
