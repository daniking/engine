use std::collections::HashMap;

use crate::math::{Vec2, Vec3};

use super::chunk::Chunk;

pub struct TerrainGrid {
    chunks: HashMap<Vec2<i32>, Chunk>,
}

impl TerrainGrid {
    pub fn new() -> Self {
        Self {
            chunks: HashMap::new(),
        }
    }

    pub fn insert(&mut self, pos: Vec2<i32>, chunk: Chunk) -> Option<Chunk> {
        self.chunks.insert(pos, chunk)
    }

    pub fn remove(&mut self, pos: Vec2<i32>) -> Option<Chunk> {
        self.chunks.remove(&pos)
    }

    pub fn chunk_pos(world_pos: Vec3<i32>) -> Vec2<i32> {
        Vec2::new(world_pos.x / Chunk::size().x, world_pos.z / Chunk::size().z)
    }

    pub fn iter(&self) -> impl ExactSizeIterator<Item = (&Vec2<i32>, &Chunk)> {
        self.chunks.iter()
    }
}
