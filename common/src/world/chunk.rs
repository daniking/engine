use noise::{BasicMulti, MultiFractal, NoiseFn, Perlin};
use serde::{Serialize, Deserialize};

use crate::{
    math::Vec3,
    world::block::{
        Block,
        BlockId::{self},
    },
};

pub const CHUNK_SIZE: Vec3<u32> = Vec3::new(16, 256, 16);

#[derive(Debug)]
pub enum ChunkError {
    OutOfBounds,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Chunk {
    blocks: Vec<Block>,
}

fn map_range(val: f64, in_min: f64, in_max: f64, out_min: f64, out_max: f64) -> f64 {
    (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
}

impl Chunk {
    pub fn with_noise(chunk_pos: ChunkPos) -> Self {
        let multi: BasicMulti<Perlin> = BasicMulti::new(154).set_octaves(7)
            .set_frequency(2.0);
        let mut blocks = vec![];
        for x in 0..CHUNK_SIZE.x {
            for y in 0..CHUNK_SIZE.y {
                for z in 0..CHUNK_SIZE.z {
                    let world_x = x as f64 + chunk_pos.x as f64 * CHUNK_SIZE.x as f64;
                    let world_z = z as f64 + chunk_pos.z as f64 * CHUNK_SIZE.z as f64;
                    let scale = 70.0;

                    let max_height = map_range(
                        multi.get([world_x / scale, world_z / scale]),
                        -1.0,
                        1.0,
                        0.0,
                        1.0,
                    ) * 256.0;

                    let stone_max_height = map_range(
                        multi.get([world_x / scale, world_z / scale]),
                        -1.0,
                        1.0,
                        0.0,
                        1.0,
                    ) * 256.0;

                    let stone_height = (stone_max_height * max_height) / 256.0;

                    let max_height = max_height as u32;

                    if y < stone_height as u32 {
                        blocks.push(Block::new(BlockId::Stone));
                    } else if y < max_height {
                        blocks.push(Block::new(BlockId::Dirt));
                    } else if y == max_height {
                        blocks.push(Block::new(BlockId::GrassTop));
                    } else {
                        blocks.push(Block::new(BlockId::Air));
                    }
                }
            }
        }
        Self { blocks }
    }

    /// Create a new [`Chunk`] filled with the given block.
    pub fn filled_with(block: Block) -> Self {
        Self {
            blocks: vec![block; CHUNK_SIZE.product() as usize],
        }
    }

    pub fn iter_pos(&self) -> ChunkIter {
        ChunkIter {
            pos: Vec3::zero(),
            size: CHUNK_SIZE,
        }
    }

    /// Get the index of the block at the given position in the chunk.
    ///
    /// It is essentially mapping a 3D position to a 1D index.
    pub fn index_at(pos: Vec3<i32>) -> Option<usize> {
        if pos.is_any_negative() {
            return None;
        }

        let size = CHUNK_SIZE.map(|f| f as i32);

        if pos.map2(size, |p, limit| p > limit).reduce_and() {
            return None;
        }

        Some((pos.x + pos.y * size.x + pos.z * size.x * size.y) as usize)
    }

    // Get a reference to the block at the given position in the chunk.
    pub fn get(&self, pos: Vec3<i32>) -> Result<&Block, ChunkError> {
        Self::index_at(pos)
            .and_then(|i| self.blocks.get(i))
            .ok_or(ChunkError::OutOfBounds)
    }

    pub fn is_empty_at(&self, at: Vec3<i32>) -> bool {
        if at.is_any_negative()
            || at
                .map2(CHUNK_SIZE, |p, limit| p >= limit as i32)
                .reduce_or()
        {
            return true;
        }
        self.get(at).map(|block| block.is_air()).unwrap_or(true)
    }

    pub fn set(&mut self, pos: Vec3<i32>, block: Block) -> Result<(), ChunkError> {
        Self::index_at(pos)
            .and_then(|i| self.blocks.get_mut(i))
            .map(|old_block| *old_block = block)
            .ok_or(ChunkError::OutOfBounds)
    }

    pub fn size() -> Vec3<i32> {
        CHUNK_SIZE.map(|f| f as i32)
    }
}

pub struct ChunkIter {
    pos: Vec3<u32>,
    size: Vec3<u32>,
}

impl Iterator for ChunkIter {
    type Item = Vec3<i32>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut pos = self.pos;
        if pos.x == self.size.x {
            pos.x = 0;
            pos.y += 1;
            if pos.y == self.size.y {
                pos.y = 0;
                pos.z += 1;
                if pos.z == self.size.z {
                    return None;
                }
            }
        }
        self.pos = pos + Vec3::unit_x();
        Some(pos.map(|f| f as i32))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ChunkPos {
    pub x: i32,
    pub z: i32,
}
impl ChunkPos {
    pub fn new(x: i32, z: i32) -> Self {
        Self { x, z }
    }

    pub fn into_vec3(self) -> Vec3<i32> {
        Vec3::new(self.x, 0, self.z)
    }
}
