use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum BlockId {
    Air,
    Dirt,
    Grass,
    GrassTop,
    Stone,
}

impl BlockId {
    #[inline]
    pub const fn is_air(&self) -> bool {
        matches!(self, Self::Air)
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Block {
    id: BlockId,
}

impl Block {
    #[inline]
    pub const fn new(id: BlockId) -> Self {
        Self { id }
    }

    #[inline]
    pub const fn air() -> Self {
        Self::new(BlockId::Air)
    }

    #[inline]
    pub const fn is_air(&self) -> bool {
        self.id.is_air()
    }

    pub fn id(&self) -> BlockId {
        self.id
    }
}
