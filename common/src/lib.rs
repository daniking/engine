use std::time::Duration;

use apecs::{Components, Entities, Entity, IsResource, World};

use comp::Time;
use math::Vec2;
use resources::{DeltaTime, GameMode, PlayerEntity};
use tracing::info;
use uid::{EntityMap, Uid};
use world::{chunk::Chunk, terrain::TerrainGrid};

use self::math::Vec3;

pub mod comp;
pub mod math;
pub mod net;
pub mod resources;
pub mod sys;
pub mod time;
pub mod uid;
pub mod world;

pub struct State {
    world: World,
}

impl State {
    pub fn new(mode: GameMode) -> Self {
        let mut world = World::default();
        world.with_resource(EntityMap::new()).unwrap();
        world.with_resource(PlayerEntity(None)).unwrap();
        world.with_resource(DeltaTime(0.0)).unwrap();
        world.with_resource(Time(0.0)).unwrap();
        world.with_resource(mode).unwrap();
        world.with_resource(TerrainGrid::new()).unwrap();
        Self { world }
    }

    pub fn tick(&mut self, dt: Duration) {
        // TODO: Check for max delta time > some threshold?
        self.world.resource_mut::<DeltaTime>().unwrap().0 = dt.as_secs_f32();
        self.world.resource_mut::<Time>().unwrap().0 += dt.as_secs_f64();

        let grid = self.world.resource::<TerrainGrid>().unwrap();

        if let Err(e) = self.world.tick() {
            tracing::error!("Failed to tick: {}", e);
        }
    }

    pub fn insert_chunk(&mut self, pos: Vec2<i32>, chunk: Chunk) {
        self.world
            .resource_mut::<TerrainGrid>()
            .unwrap()
            .insert(pos, chunk);
        // TODO: handle terrain changes
    }

    pub fn remove_chunk(&mut self, pos: Vec2<i32>) {
        self.world
            .resource_mut::<TerrainGrid>()
            .unwrap()
            .remove(pos);
    }

    /// Read a resource from the world. Returns None if the resource does not exist.
    pub fn read_resource<T: IsResource>(&self) -> &T {
        self.world
            .resource::<T>()
            .expect("Failed to read resource.")
    }

    pub fn read_resource_mut<T: IsResource>(&mut self) -> &mut T {
        self.world
            .resource_mut::<T>()
            .expect("Failed to read resource. It's probably not registered")
    }

    /// Write to a resource in the world. Panics if the resource does not exist.
    pub fn write_resource<T: IsResource>(&mut self, res: T) {
        match self.world.resource_mut::<T>() {
            Ok(t) => *t = res,
            Err(err) => {
                tracing::error!("Failed to write resource: {}", err);
            }
        }
    }

    pub fn find_entity(&self, uid: Uid) -> Option<Entity> {
        info!("Finding entity with uid: {}", uid.0);
        let ent = self.world().resource::<Entities>().unwrap();
        ent.hydrate(uid.0 as usize)
    }

    pub fn components(&self) -> &Components {
        self.world.resource::<Components>().unwrap()
    }

    /// Write to a component in the world. Panics if the entity does not have the component.
    pub fn write_component<T: Send + Sync + 'static>(&mut self, entity_id: usize, cmp: T) {
        let mut query = self.world.query::<&mut T>();
        let Some(entry) = query.find_one(entity_id) else {
            tracing::error!("Failed to write component: entity {} does not have the component {} ", entity_id, std::any::type_name::<T>());
            return;
        };
        *entry.value_mut() = cmp;
    }

    pub fn world(&self) -> &apecs::World {
        &self.world
    }

    pub fn world_mut(&mut self) -> &mut apecs::World {
        &mut self.world
    }

    pub fn get_time(&mut self) -> f64 {
        self.world.resource::<Time>().unwrap().0
    }

    pub fn terrain(&self) -> &TerrainGrid {
        self.world.resource::<TerrainGrid>().unwrap()
    }
}

/// A macro that wraps [tracing::span!] for convenience that lets you define spans with less boilerplate.
///
/// ## Examples
/// * This will create a span named "my span" with [`tracing::Level::INFO`]:
/// ```rust
/// use common::span;
/// span!(guard, "my module");
/// ```
/// * This will create a span named "my crate" with [`tracing::Level::DEBUG`]:
/// ```rust
/// use common::span;
/// // Note that there is no need to import the tracing::Level enum.
/// span!(guard, DEBUG, "my crate");
/// ```
/// * This will create a span named "spawn" with [`tracing::Level::INFO`] and a field named "entity_count" with the value 5:
/// ```rust
/// use common::span;
/// span!(guard, TRACE, "spawn", entity_count = 5);    
/// ```
#[macro_export]
macro_rules! span {
    ($guard:tt, $name:expr) => {
        let span = tracing::span!(tracing::Level::INFO, $name);
        let _guard = span.enter();
    };
    ($guard:tt, $level:ident, $name:expr) => {
        let span = tracing::span!(tracing::Level::$level, $name);
        let $guard = span.enter();
    };
    ($guard:tt, $level:ident, $name:expr, $($fields:tt)*) => {
        let span = tracing::span!(tracing::Level::$level, $name, $($fields)*);
        let $guard = span.enter();
    };
}
/// Initialize tracing and logging.
///
/// The default logging level is `INFO`.
pub fn setup_tracing() {
    // TODO: Add tracing filters
    // TODO: Write logs to a file
    tracing_subscriber::fmt()
        .with_file(true)
        .with_line_number(true)
        .with_max_level(tracing::Level::INFO)
        .init();

    let level = tracing::level_filters::LevelFilter::current()
        .to_string()
        .to_uppercase();

    tracing::info!("Logging level: {}", level);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    Top,
    Bottom,
    Left,
    Right,
    Front,
    Back,
}
impl Direction {
    pub fn vec(&self) -> Vec3<i32> {
        match self {
            Direction::Top => Vec3::new(0, 1, 0),
            Direction::Bottom => Vec3::new(0, -1, 0),
            Direction::Left => Vec3::new(-1, 0, 0),
            Direction::Right => Vec3::new(1, 0, 0),
            Direction::Front => Vec3::new(0, 0, 1),
            Direction::Back => Vec3::new(0, 0, -1),
        }
    }

    pub fn opposite(&self) -> Self {
        match self {
            Direction::Top => Direction::Bottom,
            Direction::Bottom => Direction::Top,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
            Direction::Front => Direction::Back,
            Direction::Back => Direction::Front,
        }
    }

    pub fn iter() -> impl ExactSizeIterator<Item = Direction> {
        [
            Direction::Top,
            Direction::Bottom,
            Direction::Left,
            Direction::Right,
            Direction::Front,
            Direction::Back,
        ]
        .into_iter()
    }
}
