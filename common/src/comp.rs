use std::net::SocketAddr;

use serde::{Deserialize, Serialize};

use crate::math::Vec3;

// Used to tag each player that joins the game
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Player {
    pub addr: SocketAddr,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct BlockPos(pub Vec3<f32>);

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Velocity(pub Vec3<f32>);

#[derive(Clone, Copy, Debug, Default, Serialize, Deserialize)]
pub struct MoveControl {
    pub dir: Vec3<f32>,
}

impl MoveControl {
    pub fn replace_with(&mut self, other: Self) {
        self.dir = other.dir;
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Default)]
pub struct Time(pub f64);
