use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy, Serialize, Deserialize, Default)]
pub struct Uid(pub u64);

impl From<Uid> for u64 {
    fn from(uid: Uid) -> u64 {
        uid.0
    }
}
impl From<u64> for Uid {
    fn from(uid: u64) -> Self {
        Self(uid)
    }
}

impl std::fmt::Display for Uid {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

type Entity = apecs::Entity;

#[derive(Default)] // Required to be fetched as a resource
pub struct EntityMap {
    entities: HashMap<Uid, Entity>,
    next_uid: Uid,
}

impl EntityMap {
    pub fn new() -> Self {
        Self {
            entities: HashMap::new(),
            next_uid: Uid(0),
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = (&Uid, &Entity)> {
        self.entities.iter()
    }

    pub fn get(&self, id: Uid) -> Option<&Entity> {
        self.entities.get(&id)
    }

    pub fn get_mut(&mut self, id: Uid) -> Option<&mut Entity> {
        self.entities.get_mut(&id)
    }

    pub fn remove(&mut self, k: &Uid) -> Option<Entity> {
        if let Some(entity) = self.entities.remove(k) {
            Some(entity)
        } else {
            tracing::error!("The provided key: {:?} does not exist in the entity map", k);
            None
        }
    }

    pub fn add(&mut self, k: Uid, v: Entity) {
        if let Some(_prev) = self.entities.insert(k, v) {
            Self::is_present::<Entity>();
        }
    }

    /// Incrementally add an entity to the map, returning the uid
    pub fn add_inc(&mut self, v: Entity) -> Uid {
        let uid = self.inc();
        self.add(uid, v);
        uid
    }

    /// Peek at the next uid that will be used
    pub fn peek_next(&self) -> Uid {
        self.next_uid
    }

    fn inc(&mut self) -> Uid {
        let uid = self.next_uid;
        self.next_uid.0 += 1;
        uid
    }

    fn is_present<T>() {
        let kind = std::any::type_name::<T>();
        tracing::error!("The provided {} already exists in the entity map", kind);
    }
}
