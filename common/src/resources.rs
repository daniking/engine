use apecs::Entity;

#[derive(Debug, Default, Clone)]
pub struct PlayerEntity(pub Option<Entity>);

#[derive(Debug, Default, Clone)]
pub struct DeltaTime(pub f32);

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum GameMode {
    Singleplayer,
    Client,
    Server,
}
