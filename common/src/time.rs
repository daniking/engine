use std::time::{Duration, Instant};

pub struct Time {
    last_duration: Instant,
    delta: Duration,
}

impl Time {
    pub fn new() -> Self {
        Self {
            last_duration: Instant::now(),
            delta: Duration::from_secs(0),
        }
    }

    pub fn tick(&mut self) {
        self.delta = self.last_duration.elapsed();
        self.last_duration = Instant::now();
    }

    pub fn dt(&self) -> Duration {
        self.delta
    }

    pub fn fps(&self) -> f32 {
        1. / self.delta.as_secs_f32()
    }
}
