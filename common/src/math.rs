pub type Vec2<T> = vek::Vec2<T>;
pub type Vec3<T> = vek::Vec3<T>;
pub type Mat4<T> = vek::Mat4<T>;
