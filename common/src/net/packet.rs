use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{
    comp::{self, BlockPos, Player},
    uid::Uid, math::Vec2,
};

/// Client to server packets
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ClientPacket {
    Connect,
    /// The client has moved around
    ControlUpdate {
        uid: Uid,
        control: comp::MoveControl,
    },
    PlayerUpdate {
        uid: Uid,
        pos: comp::BlockPos,
    },
    LoadChunk {
        pos: Vec2<i32>
    },
    Disconnect(Uid),
    Ping(PingKind),
}

/// Server to client packets
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ServerPacket {
    PlayerSync {
        uid: Uid,
        spawn: BlockPos,
        velocity: comp::Velocity,
        control: comp::MoveControl,
    },
    PlayerListUpdate(PlayerListAction),
    PlayerUpdate {
        uid: Uid,
        pos: comp::BlockPos,
    },
    Ping(PingKind),
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum PingKind {
    Ping,
    Pong,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum PlayerListAction {
    // Sync the list for player joining
    Sync(HashMap<Uid, Player>),
    // A new player has joined
    Add(Uid, Player),
    // A player has left
    Remove(Uid),
}
