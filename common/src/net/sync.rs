use serde::{Deserialize, Serialize};

use crate::comp;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StateSyncPacket<C> {
    pub uid: usize,
    pub components: Vec<C>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum ComponentPacket {
    Pos(comp::BlockPos),
}

pub struct Sync {
    uid: u64,
}
