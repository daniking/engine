use apecs::{anyhow::Result, ok, Query, Read, ShouldContinue};

use crate::{
    comp::{MoveControl, Velocity},
    resources::DeltaTime,
};

pub fn update_input(
    (query, _dt): (Query<(&mut Velocity, &MoveControl)>, Read<DeltaTime>),
) -> Result<ShouldContinue> {
    let mut query = query.query();
    if let Some(entry) = query.iter_mut().last() {
        let (vel, control) = entry;
        vel.0 = control.dir;
    }
    ok()
}
