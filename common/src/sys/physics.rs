use apecs::{anyhow::Result, Query, Read, ShouldContinue};

use crate::{
    comp::{BlockPos, Velocity},
    resources::DeltaTime,
};

pub fn update_physics(
    (query, dt): (Query<(&mut BlockPos, &Velocity)>, Read<DeltaTime>),
) -> Result<ShouldContinue> {
    let mut query = query.query();
    for pos in query.iter_mut() {
        let (pos, vel) = pos;
        pos.0 += vel.0 * dt.0 * 40.0;
    }
    apecs::ok()
}
