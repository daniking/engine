struct Globals {
    view_mat: mat4x4<f32>,
    proj_mat: mat4x4<f32>,
}

@group(0) @binding(0)
var<uniform> globals: Globals;

struct VertexInput {
    @location(0) pos: vec3<f32>,
    @location(1) uv: vec2<f32>,
    @location(2) norm: vec3<f32>,
}

struct VertexOutput {
    @builtin(position) vertex_pos: vec4<f32>,
    @location(0) uv: vec2<f32>,
    @location(1) norm: vec3<f32>,
}

@vertex
fn vs_main(in: VertexInput) -> VertexOutput {
    var output: VertexOutput;
    output.vertex_pos = globals.proj_mat * globals.view_mat * vec4<f32>(in.pos, 1.0);
    output.uv = in.uv;
    output.norm = in.norm;
    return output;
}

@group(0) @binding(1)
var texture: texture_2d<f32>;
@group(0) @binding(2)
var tex_sampler: sampler;

@fragment
fn fs_main(input: VertexOutput) -> @location(0) vec4<f32> {
    // Ambient lighting
    let light_color = vec3<f32>(1.0, 1.0, 1.0);
    let ambient_strength = 0.4;
    let ambient = ambient_strength * light_color;
    // Diffuse lighting
    let sun_dir = normalize(vec3<f32>(1.1, 1.2, 1.1));
    let angle = max(dot(input.norm, sun_dir), 0.0);
    let diffuse = angle * light_color;
    let obj_color = textureSample(texture, tex_sampler, input.uv);
    return vec4<f32>(obj_color.rgb * (diffuse + ambient), 1.0);
}